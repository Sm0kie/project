package rest.resources.asm;

import core.models.entities.RentingDetails;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.RentingDetailsController;
import rest.resources.RentingDetailsResource;
import rest.resources.RentingDetailsResourceList;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public class RentingDetailsResourceListAsm extends ResourceAssemblerSupport<List<RentingDetails>,RentingDetailsResourceList> {
    public RentingDetailsResourceListAsm(){
        super(RentingDetailsController.class,RentingDetailsResourceList.class);
    }
    @Override
    public RentingDetailsResourceList toResource(List<RentingDetails> rentingDetails) {
        List<RentingDetailsResource> resList = new RentingDetailsResourceAsm().toResources(rentingDetails);
        RentingDetailsResourceList finalRes = new RentingDetailsResourceList();
        finalRes.setRentingDetails(resList);
        return finalRes;
    }
}
