package core.dao;

import core.models.entities.Car;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public interface CarDao {
    List<Car> findAllCars();
    Car findCarById(Long id);
    Car createCar(Car data);
    Car updateCar(Long carId,Car data);
    Car removeCar(Long carId);
}
