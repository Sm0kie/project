package rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.models.ClientRent;
import core.models.RentingDetails;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.ClientResource;
import rest.resources.RentingDetailsResource;
import rest.resources.RentingDetailsResourceList;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
public class RentingDetailsController {

    @RequestMapping(value="/rentingDetails",method= RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<RentingDetailsResourceList> findAllRentingDetails() throws IOException {
        RentingDetailsResourceList rentingDetails;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/rentingDetails");
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response=client.execute(new HttpGet(uri));
        rentingDetails=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),RentingDetailsResourceList.class);
        return new ResponseEntity<RentingDetailsResourceList>(rentingDetails, HttpStatus.OK);
    }

    @RequestMapping(value="/rentingDetails/{rentingDetailsId}",method=RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<RentingDetailsResource> findRentingDetailsById(
            @PathVariable Long rentingDetailsId) throws IOException {
        RentingDetailsResource res;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/rentingDetails/"+rentingDetailsId);
        URI uri = null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response=client.execute(new HttpGet(uri));
        res=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),RentingDetailsResource.class);
        if(res.getRid()==0)
            return new ResponseEntity<RentingDetailsResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<RentingDetailsResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value="/rentingDetails/{rentingDetailsId}",method=RequestMethod.DELETE)
    @PreAuthorize("permitAll")
    public ResponseEntity<RentingDetailsResource> removeRentingDetails(
            @PathVariable Long rentingDetailsId) throws IOException {
        RentingDetailsResource res;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/rentingDetails/"+rentingDetailsId);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response=client.execute(new HttpDelete(uri));
        res=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),RentingDetailsResource.class);
        if(res.getRid()==0)
            return new ResponseEntity<RentingDetailsResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<RentingDetailsResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value="/addRentingDetails/{carId}",method=RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<RentingDetailsResource> addRentingDetails(
            @PathVariable Long carId,
            @RequestBody ClientRent clientRent,
            UriComponentsBuilder ucBuilder) throws IOException {
        RentingDetailsResource res;
        ClientResource clientResource;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/clients");
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpPost request = new HttpPost(uri);
        StringEntity stringEntity= new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(clientRent.getClient()))).toString(), HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response=client.execute(request);
        clientResource=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),ClientResource.class);
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/clients/"+clientResource.getRid()+"/addRentingDetails/"+carId);
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        stringEntity=new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(clientRent.getRentingDetails()))).toString(), HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request=new HttpPost(uri);
        request.setEntity(stringEntity);
        response = client.execute(request);
        res=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),RentingDetailsResource.class);
        if(res.getRid()==0)
            return new ResponseEntity<RentingDetailsResource>(HttpStatus.NOT_FOUND);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rentingDetails/{rentingDetailsId}").buildAndExpand(res.getId()).toUri());
        return new ResponseEntity<RentingDetailsResource>(res,headers,HttpStatus.OK);
    }

    @RequestMapping(value="/rentingDetails/{rentingDetailsId}",method=RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<RentingDetailsResource> updateRentingDetails(
            @PathVariable Long rentingDetailsId,
            @RequestBody RentingDetails data) throws IOException {
        RentingDetailsResource res;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/rentingDetails/"+rentingDetailsId);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpPost request = new HttpPost(uri);
        StringEntity stringEntity=new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(data))).toString(),HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response = client.execute(request);
        res=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),RentingDetailsResource.class);
        if(res.getRid()==0)
            return new ResponseEntity<RentingDetailsResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<RentingDetailsResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value = "/contract/{rentingDetailsId}", method = RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<RentingDetailsResource> createContract(
            @PathVariable Long rentingDetailsId) throws IOException {
        RentingDetailsResource res;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/contract/"+rentingDetailsId);
        URI uri = null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response=client.execute(new HttpPost(uri));
        res=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),RentingDetailsResource.class);
        if(res.getRid()==0)
            return new ResponseEntity<RentingDetailsResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<RentingDetailsResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value = "/exportPdf/{pdfId}", method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public void uploadPdf(
            @PathVariable Long pdfId,HttpServletResponse response) throws IOException {
        RentingDetailsResource res;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/rentingDetails/"+pdfId);
        URI uri = null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse responses=client.execute(new HttpGet(uri));
        res=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(responses.getEntity().getContent())),RentingDetailsResource.class);
        File f=new File("D:\\Java\\Project\\CarRentalServer\\Contract_For_"+res.getClient().getFirstName()+"_"+res.getClient().getLastName()+"_Rental_No_"+res.getRid()+".pdf");
        InputStream is=new FileInputStream(f);
        IOUtils.copy(is,response.getOutputStream());
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition","inline; filename=Contract_For_"+res.getClient().getFirstName()+"_"+res.getClient().getLastName()+"_Rental_No_"+res.getRid()+".pdf");
        response.flushBuffer();
    }
}
