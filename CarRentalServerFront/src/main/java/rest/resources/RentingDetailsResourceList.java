package rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public class RentingDetailsResourceList extends ResourceSupport {
    private List<RentingDetailsResource> rentingDetails=new ArrayList<RentingDetailsResource>();

    public List<RentingDetailsResource> getRentingDetails() {
        return rentingDetails;
    }

    public void setRentingDetails(List<RentingDetailsResource> rentingDetails) {
        this.rentingDetails = rentingDetails;
    }
}
