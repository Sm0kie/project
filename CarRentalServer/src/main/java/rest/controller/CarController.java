package rest.controller;

import core.dao.CarDao;
import core.dao.RentingDetailsDao;
import core.models.entities.Car;
import core.models.entities.RentingDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.CarResource;
import rest.resources.CarResourceList;
import rest.resources.asm.CarResourceAsm;
import rest.resources.asm.CarResourceListAsm;

import java.util.Iterator;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
public class CarController {
    @Autowired
    private CarDao carDao;
    @Autowired
    private RentingDetailsDao rentingDetailsDao;
    @RequestMapping(value="/cars",method= RequestMethod.GET)
    public ResponseEntity<CarResourceList> findAllCars(){
        CarResourceList carResourceList=new CarResourceListAsm().toResource(carDao.findAllCars());
        if(carResourceList.getCars().isEmpty())
            return new ResponseEntity<CarResourceList>(carResourceList, HttpStatus.NOT_FOUND);
        return new ResponseEntity<CarResourceList>(carResourceList,HttpStatus.OK);
    }

    @RequestMapping(value="/cars/{carId}",method=RequestMethod.GET)
    public ResponseEntity<CarResource> findCarById(
            @PathVariable Long carId) {
        Car car=carDao.findCarById(carId);
        CarResource res;
        if (car == null) {
            car=new Car();
            car.setId(0l);
            car.setModel("0");
            car.setImageName("0");
            car.setFuelType("0");
            car.setDoors("0");
            car.setAvailable(false);
            car.setCategory("0");
            res=new CarResourceAsm().toResource(car);
            return new ResponseEntity<CarResource>(res,HttpStatus.NOT_FOUND);
        }
        res=new CarResourceAsm().toResource(car);
        return new ResponseEntity<CarResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value="/cars/{carId}",method=RequestMethod.DELETE)
    public ResponseEntity<CarResource> removeCar(
            @PathVariable Long carId){
        List<RentingDetails> rentingDetails=rentingDetailsDao.findRentingDetailsByCar(carId);
        Iterator<RentingDetails> it= rentingDetails.iterator();
        if(rentingDetails!=null){
            while(it.hasNext()){
                RentingDetails rentingDet=it.next();
                rentingDetailsDao.removeRentingDetails(rentingDet.getId());
            }
        }
        Car car = carDao.removeCar(carId);
        if(car==null)
            return new ResponseEntity<CarResource>(HttpStatus.NOT_FOUND);
        CarResource carResource=new CarResourceAsm().toResource(car);
        return new ResponseEntity<CarResource>(carResource,HttpStatus.OK);
    }

    @RequestMapping(value="/cars",method=RequestMethod.POST)
    public ResponseEntity<CarResource> addCar(
            @RequestBody Car data,
            UriComponentsBuilder ucBuilder){
        CarResource carResource;
        Car car=carDao.createCar(data);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/cars/{carId}").buildAndExpand(car.getId()).toUri());
        carResource=new CarResourceAsm().toResource(car);
        return new ResponseEntity<CarResource>(carResource,headers,HttpStatus.CREATED);
    }

    @RequestMapping(value="/cars/{carId}",method=RequestMethod.POST)
    public ResponseEntity<CarResource> updateCar(
            @PathVariable Long carId,
            @RequestBody Car data){
        CarResource carResource;
        Car car = carDao.updateCar(carId,data);
        if(car==null){
            car=new Car();
            car.setId(0l);
            car.setModel("0");
            car.setImageName("0");
            car.setFuelType("0");
            car.setDoors("0");
            car.setAvailable(false);
            car.setCategory("0");
            carResource=new CarResourceAsm().toResource(car);
            return new ResponseEntity<CarResource>(carResource,HttpStatus.NOT_FOUND);
        }
        carResource = new CarResourceAsm().toResource(car);
        return new ResponseEntity<CarResource>(carResource,HttpStatus.OK);
    }

    @RequestMapping(value="/returnCar/{carId}",method=RequestMethod.GET)
    public ResponseEntity<CarResource> returnCar(
            @PathVariable Long carId) {
        Car car=carDao.findCarById(carId);
        car.setAvailable(true);
        car=carDao.updateCar(carId,car);
        CarResource res=new CarResourceAsm().toResource(car);
        return new ResponseEntity<CarResource>(res,HttpStatus.OK);
    }
}
