angular.module( 'ngBoilerplate.admin', [
  'ui.router',
  'plusOne',
  'ngBoilerplate.login',
  'ngBoilerplate.client',
  'ngBoilerplate.employee',
  'ngBoilerplate.car'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'admin_home', {
    url: '/home/admin',
    views: {
      'main': {
        controller: 'AdminHomeCtrl',
        templateUrl: 'admin/admin_home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' },
     resolve: {
         clients: function(clientInfoService) {
             return clientInfoService.getAllClients();
         }
     }
})
.state( 'employeeSearch', {
      url: '/employee',
      views: {
        'main': {
          controller: 'EmployeeCtrl',
          templateUrl: 'admin/employees.tpl.html'
        }
      },
      data:{ pageTitle: 'Employee' },
       resolve: {
           employees: function(employeeInfoService) {
               return employeeInfoService.getAllEmployees();
           }
       }
   })
.state( 'carSearch', {
      url: '/cars',
      views: {
        'main': {
          controller: 'CarCtrl',
          templateUrl: 'admin/cars.tpl.html'
        }
      },
      data:{ pageTitle: 'Cars' },
       resolve: {
           cars: function(carInfoService) {
               return carInfoService.getAllCars();
           }
       }
   });
})
.factory('adminInfoService',function($resource){
    var service={};
    service.getAdminById = function(adminId) {
            var Admin= $resource("/CarRental/admins/:paramAdminId");
            return Admin.get({paramAdminId:adminId}).$promise;
        };
    return service;
})
.controller('CarCtrl',function($scope, $state,sessionService,cars,carInfoService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.cars=cars;
    $scope.addCar=function(category,brand,model,doors,fuelType,engineSize,price,location,imageName){
        carInfoService.addCar({
            category: $scope.category,
            brand: $scope.brand,
            model: $scope.model,
            doors: $scope.doors,
            fuelType:$scope.fuelType,
            engineSize:$scope.engineSize,
            price:$scope.price,
            location:$scope.location,
            imageName:$scope.imageName,
            available:true
        }).then(function(){
            $state.go($state.current,{},{reload:true});
        });
    };
})
.controller('EmployeeCtrl',function($scope, $state,employeeInfoService,sessionService,employees){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.addEmployee=function(firstName,lastName,email,username,password){
             employeeInfoService.addEmployee({
                 firstName: firstName,
                 lastName: lastName,
                 email:email,
                 username:username,
                 password:password
             }).then(function(){
                 $state.go($state.current,{},{reload:true});
             });
         };
    $scope.employees=employees;
})
.controller( 'AdminHomeCtrl', function HomeController( $state,clients,$scope, sessionService,$resource) {
      $scope.isLoggedIn = sessionService.isLoggedIn;
         $scope.logout = sessionService.logout;
         if(!sessionService.isLoggedIn()){
             sessionService.logout();
             $state.go("login");
         }
      $scope.clients=clients;
  });