package rest.resources.asm;

import core.models.Car;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.CarController;
import rest.resources.CarResource;
import rest.resources.CarResourceList;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public class CarResourceListAsm extends ResourceAssemblerSupport<List<Car>, CarResourceList> {
    public CarResourceListAsm() {
        super(CarController.class, CarResourceList.class);
    }
    @Override
    public CarResourceList toResource(List<Car> cars) {
        List<CarResource> resList = new CarResourceAsm().toResources(cars);
        CarResourceList finalRes = new CarResourceList();
        finalRes.setCars(resList);
        return finalRes;
    }
}