package core.dao.impl;

import core.dao.ClientDao;
import core.models.entities.Client;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@Service
@Transactional
public class ClientDaoImpl implements ClientDao {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Client> findAllClients() {
        return em.createQuery("SELECT a FROM Client a").getResultList();
    }

    @Override
    public Client findClientById(Long id) {
        return em.find(Client.class,id);
    }

    @Override
    public List<Client> findClientByCNP(String CNP) {
        return em.createQuery("SELECT a FROM Client a where a.CNP=?1").setParameter(1,CNP).getResultList();
    }

    @Override
    public Client createClient(Client data) {
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }

    @Override
    public Client updateClient(Long clientId, Client data) {
        Client client = em.find(Client.class,clientId);
        client.setCNP(data.getCNP());
        client.setFirstName(data.getFirstName());
        client.setLastName(data.getLastName());
        client.setAge(data.getAge());
        client.setPhoneNumber(data.getPhoneNumber());
        client.setEmail(data.getEmail());
        em.merge(client);
        return client;
    }

    @Override
    public Client removeClient(Long id) {
        Client client=em.find(Client.class,id);
        em.remove(client);
        return client;
    }
}
