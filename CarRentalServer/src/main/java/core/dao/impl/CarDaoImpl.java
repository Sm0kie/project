package core.dao.impl;

import core.dao.CarDao;
import core.models.entities.Car;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@Service
@Transactional
public class CarDaoImpl implements CarDao {
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Car> findAllCars() {
        return em.createQuery("SELECT a FROM Car a").getResultList();
    }

    @Override
    public Car findCarById(Long id) {
        return em.find(Car.class,id);
    }

    @Override
    public Car createCar(Car data) {
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }

    @Override
    public Car updateCar(Long carId, Car data) {
        Car car=em.find(Car.class,carId);
        car.setAvailable(data.getAvailable());
        car.setBrand(data.getBrand());
        car.setCategory(data.getCategory());
        car.setDoors(data.getDoors());
        car.setEngineSize(data.getEngineSize());
        car.setFuelType(data.getFuelType());
        car.setImageName(data.getImageName());
        car.setModel(data.getModel());
        car.setPrice(data.getPrice());
        car.setLocation(data.getLocation());
        em.merge(car);
        return car;
    }

    @Override
    public Car removeCar(Long carId) {
        Car car=em.find(Car.class,carId);
        em.remove(car);
        return car;
    }
}
