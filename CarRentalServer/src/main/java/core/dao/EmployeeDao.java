package core.dao;

import core.models.entities.Employee;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public interface EmployeeDao {
    List<Employee> findAllEmployees();
    Employee findEmployeeById(Long id);
    Employee findEmployeeByUserName(String username);
    Employee createEmployee(Employee data);
    Employee updateEmployee(Long employeeId,Employee data);
    Employee removeEmployee(Long id);
}
