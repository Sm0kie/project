package rest.resources.asm;

import core.models.Admin;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.AdminController;
import rest.resources.AdminResource;

/**
 * Created by ilyes on 5/8/2016.
 */
public class AdminResourceAsm extends ResourceAssemblerSupport<Admin,AdminResource> {
    public AdminResourceAsm(){
        super(AdminController.class,AdminResource.class);
    }
    @Override
    public AdminResource toResource(Admin admin) {
        AdminResource res=new AdminResource();
        res.setRid(admin.getId());
        res.setFirstName(admin.getFirstName());
        res.setLastName(admin.getLastName());
        res.setUsername(admin.getUsername());
        res.setPassword(admin.getPassword());
        return res;
    }
}
