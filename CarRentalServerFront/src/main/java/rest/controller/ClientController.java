package rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.models.Client;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.ClientResource;
import rest.resources.ClientResourceList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
@RequestMapping(value="/clients")
public class ClientController {

    @RequestMapping(method= RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<ClientResourceList> findAllClients() throws IOException {
        ClientResourceList clients;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/clients");
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){};
        HttpResponse response = client.execute(new HttpGet(uri));
        clients=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),ClientResourceList.class);
        return new ResponseEntity<ClientResourceList>(clients, HttpStatus.OK);
    }

    @RequestMapping(value="/{clientId}",method=RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<ClientResource> findClientById(
            @PathVariable Long clientId) throws IOException {
        ClientResource clientRes;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder= new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/clients/"+clientId);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response = client.execute(new HttpGet(uri));
        clientRes =new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),ClientResource.class);
        if(clientRes.getRid()==0)
            return new ResponseEntity<ClientResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<ClientResource>(clientRes,HttpStatus.OK);
    }

    @RequestMapping(value="/{clientId}",method=RequestMethod.DELETE)
    @PreAuthorize("permitAll")
    public ResponseEntity<ClientResource> removeCar(
            @PathVariable Long clientId) throws IOException {
        ClientResource clientRes;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/clients/"+clientId);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response =client.execute(new HttpDelete(uri));
        clientRes = new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),ClientResource.class);
        return new ResponseEntity<ClientResource>(clientRes,HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<ClientResource> addClient(
            @RequestBody Client data,
            UriComponentsBuilder ucBuilder) throws IOException {
        ClientResource clientResource;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/clients");
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpPost request = new HttpPost(uri);
        StringEntity stringEntity= new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(data))).toString(), HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response=client.execute(request);
        clientResource=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),ClientResource.class);
        if(clientResource.getRid()==0)
            return new ResponseEntity<ClientResource>(HttpStatus.CONFLICT);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/clients/{clientId}").buildAndExpand(clientResource.getId()).toUri());
        return new ResponseEntity<ClientResource>(clientResource,headers,HttpStatus.OK);
    }

    @RequestMapping(value="/{clientId}",method=RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<ClientResource> updateClient(
            @PathVariable Long clientId,
            @RequestBody Client data) throws IOException {
        ClientResource res;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/clients/"+clientId);
        URI uri=null;
        try {
            uri=builder.build();
        } catch (URISyntaxException e) {}
        HttpPost request = new HttpPost(uri);
        StringEntity stringEntity=new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(data))).toString(),HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response=client.execute(request);
        res=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),ClientResource.class);
        if(res.getRid()==0)
            return new ResponseEntity<ClientResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }
}
