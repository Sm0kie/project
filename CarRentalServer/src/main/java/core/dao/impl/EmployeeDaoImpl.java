package core.dao.impl;

import core.dao.EmployeeDao;
import core.models.entities.Employee;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@Service
@Transactional
public class EmployeeDaoImpl implements EmployeeDao{
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Employee> findAllEmployees() {
        return em.createQuery("SELECT a FROM Employee a").getResultList();
    }

    @Override
    public Employee findEmployeeById(Long id) {
        return em.find(Employee.class,id);
    }

    @Override
    public Employee findEmployeeByUserName(String username) {
        Query query = em.createQuery("SELECT a from Employee a WHERE a.username=?1");
        query.setParameter(1,username);
        List<Employee> employees=query.getResultList();
        if(employees.isEmpty())
            return null;
        return employees.get(0);
    }

    @Override
    public Employee createEmployee(Employee data) {
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }

    @Override
    public Employee updateEmployee(Long employeeId, Employee data) {
        Employee employee = em.find(Employee.class,employeeId);
        employee.setLastName(data.getLastName());
        employee.setFirstName(data.getFirstName());
        employee.setPassword(data.getPassword());
        employee.setUsername(data.getUsername());
        em.merge(employee);
        return employee;
    }

    @Override
    public Employee removeEmployee(Long id) {
        Employee employee=em.find(Employee.class,id);
        em.remove(employee);
        return employee;
    }
}
