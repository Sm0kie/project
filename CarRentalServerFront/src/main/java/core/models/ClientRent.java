package core.models;

/**
 * Created by ilyes on 5/12/2016.
 */
public class ClientRent {
    private Client client;
    private RentingDetails rentingDetails;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public RentingDetails getRentingDetails() {
        return rentingDetails;
    }

    public void setRentingDetails(RentingDetails rentingDetails) {
        this.rentingDetails = rentingDetails;
    }
}
