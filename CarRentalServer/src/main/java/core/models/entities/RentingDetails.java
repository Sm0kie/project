package core.models.entities;

import javax.persistence.*;

/**
 * Created by ilyes on 5/8/2016.
 */
@Entity
public class RentingDetails {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id",nullable=false)
    private Long id;
    @Column(name="pickUpLocation")
    private String pickUpLocation;
    @Column(name="returnLocation")
    private String returnLocation;
    @Column(name="pickUpDate")
    private String pickUpDate;
    @Column(name="dropOffDate")
    private String dropOffDate;
    @Column(name="pickUpHour")
    private String pickUpHour;
    @Column(name="dropOffHour")
    private String dropOffHour;
    @ManyToOne
    private Car car;
    @ManyToOne
    private Client client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getReturnLocation() {
        return returnLocation;
    }

    public void setReturnLocation(String returnLocation) {
        this.returnLocation = returnLocation;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getDropOffDate() {
        return dropOffDate;
    }

    public void setDropOffDate(String dropOffDate) {
        this.dropOffDate = dropOffDate;
    }

    public String getPickUpHour() {
        return pickUpHour;
    }

    public void setPickUpHour(String pickUpHour) {
        this.pickUpHour = pickUpHour;
    }

    public String getDropOffHour() {
        return dropOffHour;
    }

    public void setDropOffHour(String dropOffHour) {
        this.dropOffHour = dropOffHour;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
