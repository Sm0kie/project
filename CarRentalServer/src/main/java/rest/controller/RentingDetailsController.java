package rest.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import core.dao.CarDao;
import core.dao.ClientDao;
import core.dao.RentingDetailsDao;
import core.models.entities.Car;
import core.models.entities.Client;
import core.models.entities.RentingDetails;
import org.h2.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.email.EmailAPI;
import rest.resources.RentingDetailsResource;
import rest.resources.RentingDetailsResourceList;
import rest.resources.asm.RentingDetailsResourceAsm;
import rest.resources.asm.RentingDetailsResourceListAsm;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
public class RentingDetailsController {
    @Autowired
    private RentingDetailsDao rentingDetailsDao;
    @Autowired
    private ClientDao clientDao;
    @Autowired
    private CarDao carDao;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<RentingDetailsResourceList> findAllRentingDetails() {
        List<RentingDetails> rentingDetails = rentingDetailsDao.findAllRentingDetails();
        RentingDetailsResourceList res = new RentingDetailsResourceListAsm().toResource(rentingDetails);
        if (rentingDetails.isEmpty())
            return new ResponseEntity<RentingDetailsResourceList>(res, HttpStatus.NOT_FOUND);
        return new ResponseEntity<RentingDetailsResourceList>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/rentingDetails/{rentingDetailsId}", method = RequestMethod.GET)
    public ResponseEntity<RentingDetailsResource> findRentingDetailsById(
            @PathVariable Long rentingDetailsId) {
        RentingDetails rentingDetails = rentingDetailsDao.findRentingDetailsById(rentingDetailsId);
        RentingDetailsResource res;
        if (rentingDetails == null) {
            rentingDetails = new RentingDetails();
            rentingDetails.setPickUpDate("0");
            rentingDetails.setReturnLocation("0");
            rentingDetails.setPickUpLocation("0");
            rentingDetails.setDropOffHour("0");
            rentingDetails.setDropOffDate("0");
            rentingDetails.setId(0l);
            rentingDetails.setPickUpHour("0");
            res = new RentingDetailsResourceAsm().toResource(rentingDetails);
            return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.NOT_FOUND);
        }
        res = new RentingDetailsResourceAsm().toResource(rentingDetails);
        return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/rentingDetails/{rentingDetailsId}", method = RequestMethod.DELETE)
    public ResponseEntity<RentingDetailsResource> removeRentingDetails(
            @PathVariable Long rentingDetailsId) {
        RentingDetails rentingDetails = rentingDetailsDao.removeRentingDetails(rentingDetailsId);
        if (rentingDetails == null)
            return new ResponseEntity<RentingDetailsResource>(HttpStatus.NOT_FOUND);
        RentingDetailsResource res = new RentingDetailsResourceAsm().toResource(rentingDetails);
        return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{clientId}/addRentingDetails/{carId}", method = RequestMethod.POST)
    public ResponseEntity<RentingDetailsResource> addRentingDetails(
            @PathVariable Long clientId,
            @PathVariable Long carId,
            @RequestBody RentingDetails data,
            UriComponentsBuilder ucBuilder) {
        Client client = clientDao.findClientById(clientId);
        Car car = carDao.findCarById(carId);
        if (data.getPickUpDate().compareTo(data.getDropOffDate()) > 0) {
            RentingDetailsResource res = new RentingDetailsResource();
            res.setRid(0l);
            res.setReturnLocation("0");
            return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.NOT_ACCEPTABLE);
        }
        if (data.getPickUpDate().compareTo(data.getDropOffDate()) == 0)
            if (data.getPickUpHour().compareTo(data.getDropOffHour()) < 0) {
                RentingDetailsResource res = new RentingDetailsResource();
                res.setRid(0l);
                res.setReturnLocation("0");
                return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.NOT_ACCEPTABLE);
            }
        if (!car.getAvailable()) {
            RentingDetailsResource res = new RentingDetailsResource();
            res.setRid(0l);
            res.setReturnLocation("0");
            return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.CONFLICT);
        }
        data.setCar(car);
        data.setPickUpLocation(data.getCar().getLocation());
        data.setClient(client);
        RentingDetailsResourceList rentingDetailsResourceList = new RentingDetailsResourceListAsm().toResource(rentingDetailsDao.findRentingDetailsByCar(carId));
        Iterator<RentingDetailsResource> it = rentingDetailsResourceList.getRentingDetails().iterator();
        if (!rentingDetailsResourceList.getRentingDetails().isEmpty()) {
            while (it.hasNext()) {
                RentingDetailsResource resource = it.next();
                if (resource.getCar().equals(car))
                    if(resource.getPickUpDate().compareTo(data.getPickUpDate())>0)
                    if (resource.getDropOffDate().compareTo(data.getPickUpDate()) < 0) {
                        RentingDetailsResource res = new RentingDetailsResource();
                        res.setRid(0l);
                        res.setReturnLocation("0");
                        return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.NOT_ACCEPTABLE);
                    } else if (resource.getDropOffDate().compareTo(data.getPickUpDate()) == 0)
                        if (resource.getDropOffHour().compareTo(data.getPickUpHour()) < 0)
                        {
                            RentingDetailsResource res = new RentingDetailsResource();
                            res.setRid(0l);
                            res.setReturnLocation("0");
                            return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.NOT_ACCEPTABLE);
                        }
            }
        }

        RentingDetails rentingDetails = rentingDetailsDao.createRentingDetails(data);
        RentingDetailsResource res = new RentingDetailsResourceAsm().toResource(rentingDetails);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rentingDetails/{rentingDetails}").buildAndExpand(rentingDetails.getId()).toUri());
        {
            int days=getDays(rentingDetails);
            Long price=rentingDetails.getCar().getPrice()*days;
            String confFile = "mail-bean.xml";
            ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(confFile);
            EmailAPI emailAPI = (EmailAPI) context.getBean("Email");
            String toAddr = client.getEmail();
            String fromAddr = "craz8d@gmail.com";
            String subject = "Renting details";
            String body = "This e-mail is auto-generated, please do not reply to this message.\n" +
                    "Here are the details of the rental:\n" +
                    "Customer name: " + client.getFirstName() + " " + client.getLastName() + "\n" +
                    "CNP: " + client.getCNP() + "\n" +
                    "Age: " + client.getAge() + "\n" +
                    "Phone number: " + client.getPhoneNumber() + "\n" +
                    "Car: " + car.getBrand() + " " + car.getModel() + " " +
                    car.getCategory() + " " + car.getEngineSize() + " " +
                    car.getDoors() + " " + car.getFuelType() + "\n" +
                    "Pick up date: " + rentingDetails.getPickUpDate() + "\n" +
                    "Pick up hour: " + rentingDetails.getPickUpHour() + "\n" +
                    "Pick up location: " + rentingDetails.getPickUpLocation() + "\n" +
                    "Return date: " + rentingDetails.getDropOffDate() + "\n" +
                    "Return hour: " + rentingDetails.getDropOffHour() + "\n" +
                    "Return location: " + rentingDetails.getReturnLocation() + "\n" +
                    "Price for "+days+" days: "+price+"€\n"+
                    "Thank you for using our services.";
            emailAPI.readyToSendEmail(toAddr, fromAddr, subject, body);
        }
        return new ResponseEntity<RentingDetailsResource>(res, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/rentingDetails/{rentingDetailsId}", method = RequestMethod.POST)
    public ResponseEntity<RentingDetailsResource> updateRentingDetails(
            @PathVariable Long rentingDetailsId,
            @RequestBody RentingDetails data) {
        data.setPickUpLocation(data.getCar().getLocation());
        RentingDetails rentingDetails = rentingDetailsDao.updateRentingDetails(rentingDetailsId, data);
        RentingDetailsResource res;
        if (rentingDetails == null) {
            rentingDetails = new RentingDetails();
            rentingDetails.setId(0l);
            rentingDetails.setDropOffDate("0");
            rentingDetails.setClient(new Client());
            rentingDetails.setCar(new Car());
            res = new RentingDetailsResourceAsm().toResource(rentingDetails);
            return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.NOT_FOUND);
        }
        res = new RentingDetailsResourceAsm().toResource(rentingDetails);
        return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/contract/{rentingDetailsId}", method = RequestMethod.POST)
    public ResponseEntity<RentingDetailsResource> createContract(
            @PathVariable Long rentingDetailsId) {
        RentingDetails rentingDetails = rentingDetailsDao.findRentingDetailsById(rentingDetailsId);
        RentingDetailsResource res = new RentingDetailsResourceAsm().toResource(rentingDetails);
        createPdfContract(res);
        Car car=carDao.findCarById(rentingDetails.getCar().getId());
        car.setLocation(rentingDetails.getReturnLocation());
        car.setAvailable(false);
        carDao.updateCar(car.getId(),car);
        return new ResponseEntity<RentingDetailsResource>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/exportPdf/{pdfId}", method = RequestMethod.GET)
    public void uploadPdf(
            @PathVariable Long pdfId,HttpServletResponse response) throws IOException {
        RentingDetails rentingDetails = rentingDetailsDao.findRentingDetailsById(pdfId);
        RentingDetailsResource res = new RentingDetailsResourceAsm().toResource(rentingDetails);
        IOUtils.copy(new FileInputStream("D:\\Java\\Project\\CarRentingServer\\"+"Contract_For_"+res.getClient().getFirstName()+"_"+res.getClient().getLastName()+"_Rental_No_"+res.getRid()+".pdf"),response.getOutputStream());
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition","attachment; filename=somefile.pdf");
        response.flushBuffer();

    }

    private int getDays(RentingDetails rentingDetails){
        int pickUpYear=Integer.parseInt(rentingDetails.getPickUpDate().substring(0,4));
        int dropOffYear=Integer.parseInt(rentingDetails.getDropOffDate().substring(0,4));
        int pickUpMonth=Integer.parseInt(rentingDetails.getPickUpDate().substring(5,7));
        int dropOffMonth=Integer.parseInt(rentingDetails.getDropOffDate().substring(5,7));
        int pickUpDay=Integer.parseInt(rentingDetails.getPickUpDate().substring(8));
        int dropOffDay=Integer.parseInt(rentingDetails.getDropOffDate().substring(8));
        int days;
        if(pickUpYear==dropOffYear){
            if(pickUpMonth==dropOffMonth){
                if(pickUpDay==dropOffDay)
                    days=1;
                else{
                    days=dropOffDay-pickUpDay+1;
                }
            }else{
                Integer remainingDays;
                switch(pickUpMonth){
                    case 1:
                       remainingDays=31-pickUpDay;
                       break;
                    case 2:
                        remainingDays=28-pickUpDay;
                        break;
                    case 3:
                        remainingDays=31-pickUpDay;
                        break;
                    case 4:
                        remainingDays=30-pickUpDay;
                        break;
                    case 5:
                        remainingDays=31-pickUpDay;
                        break;
                    case 6:
                        remainingDays=30-pickUpDay;
                        break;
                    case 7:
                        remainingDays=31-pickUpDay;
                        break;
                    case 8:
                        remainingDays=31-pickUpDay;
                        break;
                    case 9:
                        remainingDays=30-pickUpDay;
                        break;
                    case 10:
                        remainingDays=31-pickUpDay;
                        break;
                    case 11:
                        remainingDays=30-pickUpDay;
                        break;
                    default:
                        remainingDays=31-pickUpDay;
                        break;
                }
                Integer month=dropOffMonth-pickUpMonth-1;
                days=dropOffDay-1+remainingDays+month*30;
            }
        }else{
            Integer beforeMonth=12-pickUpMonth;
            Integer afterMonth=dropOffMonth-1;
            Integer remainingDays;
            switch(pickUpMonth){
                case 1:
                    remainingDays=31-pickUpDay;
                    break;
                case 2:
                    remainingDays=28-pickUpDay;
                    break;
                case 3:
                    remainingDays=31-pickUpDay;
                    break;
                case 4:
                    remainingDays=30-pickUpDay;
                    break;
                case 5:
                    remainingDays=31-pickUpDay;
                    break;
                case 6:
                    remainingDays=30-pickUpDay;
                    break;
                case 7:
                    remainingDays=31-pickUpDay;
                    break;
                case 8:
                    remainingDays=31-pickUpDay;
                    break;
                case 9:
                    remainingDays=30-pickUpDay;
                    break;
                case 10:
                    remainingDays=31-pickUpDay;
                    break;
                case 11:
                    remainingDays=30-pickUpDay;
                    break;
                default:
                    remainingDays=31-pickUpDay;
                    break;
            }
            days=(pickUpYear-dropOffYear-1)*365+remainingDays+dropOffDay-1+(beforeMonth+afterMonth-1)*30;
        }
        return days;
    }
    private int getDays(RentingDetailsResource rentingDetails){
        int pickUpYear=Integer.parseInt(rentingDetails.getPickUpDate().substring(0,4));
        int dropOffYear=Integer.parseInt(rentingDetails.getDropOffDate().substring(0,4));
        int pickUpMonth=Integer.parseInt(rentingDetails.getPickUpDate().substring(5,7));
        int dropOffMonth=Integer.parseInt(rentingDetails.getDropOffDate().substring(5,7));
        int pickUpDay=Integer.parseInt(rentingDetails.getPickUpDate().substring(8));
        int dropOffDay=Integer.parseInt(rentingDetails.getDropOffDate().substring(8));
        int days;
        if(pickUpYear==dropOffYear){
            if(pickUpMonth==dropOffMonth){
                if(pickUpDay==dropOffDay)
                    days=1;
                else{
                    days=dropOffDay-pickUpDay+1;
                }
            }else{
                Integer remainingDays;
                switch(pickUpMonth){
                    case 1:
                        remainingDays=31-pickUpDay;
                        break;
                    case 2:
                        remainingDays=28-pickUpDay;
                        break;
                    case 3:
                        remainingDays=31-pickUpDay;
                        break;
                    case 4:
                        remainingDays=30-pickUpDay;
                        break;
                    case 5:
                        remainingDays=31-pickUpDay;
                        break;
                    case 6:
                        remainingDays=30-pickUpDay;
                        break;
                    case 7:
                        remainingDays=31-pickUpDay;
                        break;
                    case 8:
                        remainingDays=31-pickUpDay;
                        break;
                    case 9:
                        remainingDays=30-pickUpDay;
                        break;
                    case 10:
                        remainingDays=31-pickUpDay;
                        break;
                    case 11:
                        remainingDays=30-pickUpDay;
                        break;
                    default:
                        remainingDays=31-pickUpDay;
                        break;
                }
                Integer month=dropOffMonth-pickUpMonth-1;
                days=dropOffDay-1+remainingDays+month*30;
            }
        }else{
            Integer beforeMonth=12-pickUpMonth;
            Integer afterMonth=dropOffMonth-1;
            Integer remainingDays;
            switch(pickUpMonth){
                case 1:
                    remainingDays=31-pickUpDay;
                    break;
                case 2:
                    remainingDays=28-pickUpDay;
                    break;
                case 3:
                    remainingDays=31-pickUpDay;
                    break;
                case 4:
                    remainingDays=30-pickUpDay;
                    break;
                case 5:
                    remainingDays=31-pickUpDay;
                    break;
                case 6:
                    remainingDays=30-pickUpDay;
                    break;
                case 7:
                    remainingDays=31-pickUpDay;
                    break;
                case 8:
                    remainingDays=31-pickUpDay;
                    break;
                case 9:
                    remainingDays=30-pickUpDay;
                    break;
                case 10:
                    remainingDays=31-pickUpDay;
                    break;
                case 11:
                    remainingDays=30-pickUpDay;
                    break;
                default:
                    remainingDays=31-pickUpDay;
                    break;
            }
            days=(pickUpYear-dropOffYear-1)*365+remainingDays+dropOffDay-1+(beforeMonth+afterMonth-1)*30;
        }
        return days;
    }

    private void createPdfContract(RentingDetailsResource res){
        Document document= new Document();
        int days=getDays(res);
        Long price=res.getCar().getPrice()*days;
        document.setPageSize(PageSize.A4);
        try{
            PdfWriter.getInstance(document,new FileOutputStream("Contract_For_"+res.getClient().getFirstName()+"_"+res.getClient().getLastName()+"_Rental_No_"+res.getRid()+".pdf"));
            document.open();
            Paragraph paragraph=new Paragraph();
            paragraph.add("RENTAL AGREEMENT No. : "+res.getRid()+"\nDATE: "+ LocalDate.now().getDayOfMonth()+"-"+LocalDate.now().getMonth()+"-"+LocalDate.now().getYear()+"\n\n\n" +
                    "\t\tMOTOR VEHICLE RENTAL AGREEMENT\n\n" +
                    "NAME: "+res.getClient().getFirstName()+" "+res.getClient().getLastName()+
                    "\nRENTED BY: "+res.getPickUpDate()+"\nRETURN DATE: "+res.getDropOffDate()+
                    "\n\nI agree to rent the vehicle model: "+res.getCar().getBrand()+" "+
                    res.getCar().getModel()+" for the price of "+price+" €, the lease beginning with the date of "+res.getPickUpDate()+
                    ", at hour "+res.getPickUpHour()+", the car being picked up at the location "+res.getPickUpLocation()+
                    ", until the date of "+res.getDropOffDate()+", at hour "+res.getDropOffHour()+", the car being returned to the location "+res.getReturnLocation()+".\n\n\nSignature:");
            document.add(paragraph);
            document.close();
        }catch(Exception e){}
    }
}
