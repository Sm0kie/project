angular.module('templates-app', ['admin/admin_home.tpl.html', 'admin/cars.tpl.html', 'admin/employees.tpl.html', 'car/car.tpl.html', 'car/update_car.tpl.html', 'clients/clients.tpl.html', 'clients/update_client.tpl.html', 'employee/employee.tpl.html', 'employee/update_employee.tpl.html', 'employee_home/employee_home.tpl.html', 'home/home.tpl.html', 'home/rentCar.tpl.html', 'login/login.tpl.html']);

angular.module("admin/admin_home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/admin_home.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"AdminHomeCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Clients\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"search\"placeholder=\"client information\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>ID</th>\n" +
    "        <th>First Name</th>\n" +
    "        <th>LastName</th>\n" +
    "        <th>Email</th>\n" +
    "        <th>CNP</th>\n" +
    "        <th>Age</th>\n" +
    "        <th>Phone number</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr ng-repeat=\"client in clients |filter:search\">\n" +
    "            <td>{{client.rid}}</td>\n" +
    "            <td>{{client.firstName}}</td>\n" +
    "            <td>{{client.lastName}}</td>\n" +
    "            <td>{{client.email}}</td>\n" +
    "            <td>{{client.cnp}}</td>\n" +
    "            <td>{{client.age}}</td>\n" +
    "            <td>{{client.phoneNumber}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageClient({clientId:client.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("admin/cars.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/cars.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"CarCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Cars\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>Category:</label>\n" +
    "        <input type=\"text\" ng-model=\"category\" class=\"form-control\" required/>\n" +
    "        <label>Brand:</label>\n" +
    "        <input type=\"text\" ng-model=\"brand\" class=\"form-control\"required/>\n" +
    "        <label>Model:</label>\n" +
    "        <input type=\"text\" ng-model=\"model\" class=\"form-control\"required/>\n" +
    "        <label>Doors:</label>\n" +
    "        <input type=\"text\" ng-model=\"doors\" class=\"form-control\"required/>\n" +
    "        <label>Fuel Type:</label>\n" +
    "        <input type=\"text\" ng-model=\"fuelType\" class=\"form-control\"required/>\n" +
    "        <label>Engine Size:</label>\n" +
    "        <input type=\"text\" ng-model=\"engineSize\" class=\"form-control\"required/>\n" +
    "        <form name=\"Price\">\n" +
    "            <label>Price:</label>\n" +
    "            <input type=\"number\" name=\"price\" min=\"0\" class=\"form-control\" ng-model=\"price\" required/>\n" +
    "            <span style=\"color:red\" ng-show=\"Price.price.$dirty && Price.price.$invalid\">Price must be a positive value</span>\n" +
    "        </form>\n" +
    "        <label>Location:</label>\n" +
    "        <input type=\"text\" ng-model=\"location\" class=\"form-control\" required/>\n" +
    "        <label>Image source:</label>\n" +
    "        <input type=\"text\" ng-model=\"imageName\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Add\" ng-click=\"addCar(category,brand,model,doors,fuelType,engineSize,price,location,imageName)\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Cars\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"search\"placeholder=\"Car Info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>ID</th>\n" +
    "        <th>Category</th>\n" +
    "        <th>Brand</th>\n" +
    "        <th>Model</th>\n" +
    "        <th>Doors</th>\n" +
    "        <th>Fuel type</th>\n" +
    "        <th>Engine size</th>\n" +
    "        <th>Price</th>\n" +
    "        <th>Location</th>\n" +
    "        <th>Image</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr align=\"center\"valign=\"bottom\" ng-repeat=\"car in cars |filter:search\">\n" +
    "            <td>{{car.rid}}</td>\n" +
    "            <td>{{car.category}}</td>\n" +
    "            <td>{{car.brand}}</td>\n" +
    "            <td>{{car.model}}</td>\n" +
    "            <td>{{car.doors}}</td>\n" +
    "            <td>{{car.fuelType}}</td>\n" +
    "            <td>{{car.engineSize}}</td>\n" +
    "            <td>{{car.price}} &#8364</td>\n" +
    "            <td>{{car.location}}</td>\n" +
    "            <td><img ng-src=\"{{car.imageName}}\" width=\"200\" height=\"100\"></td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageCar({carId:car.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("admin/employees.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/employees.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"EmployeeCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Add new employee\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>First name:</label>\n" +
    "        <input type=\"text\" ng-model=\"firstName\" class=\"form-control\" required/>\n" +
    "        <label>Last name:</label>\n" +
    "        <input type=\"text\" ng-model=\"lastName\" class=\"form-control\"required/>\n" +
    "        <label>Username:</label>\n" +
    "        <input type=\"text\" ng-model=\"username\" class=\"form-control\"required/>\n" +
    "        <label>Password:</label>\n" +
    "        <input type=\"password\" ng-model=\"password\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Add\" ng-click=\"addEmployee(firstName,lastName,email,username,password)\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Employees\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"search\"placeholder=\"Employee Info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>ID</th>\n" +
    "        <th>First Name</th>\n" +
    "        <th>LastName</th>\n" +
    "        <th>Username</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr ng-repeat=\"employee in employees |filter:search\">\n" +
    "            <td>{{employee.rid}}</td>\n" +
    "            <td>{{employee.firstName}}</td>\n" +
    "            <td>{{employee.lastName}}</td>\n" +
    "            <td>{{employee.username}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageEmployee({employeeId:employee.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("car/car.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("car/car.tpl.html",
    "<html ng-app=\"ngBoilerplate.car\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div ng-control=\"CarManageCtrl\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Car\n" +
    "        </h1>\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{car.rid}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Category: </th>\n" +
    "            <td>{{car.category}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Brand: </th>\n" +
    "            <td>{{car.brand}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Model: </th>\n" +
    "            <td>{{car.model}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Doors: </th>\n" +
    "            <td>{{car.doors}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Fuel Type: </th>\n" +
    "            <td>{{car.fuelType}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Engine Size: </th>\n" +
    "            <td>{{car.engineSize}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Price: </th>\n" +
    "            <td>{{car.price}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Location: </th>\n" +
    "            <td>{{car.location}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>Image</p>\n" +
    "        <p >\n" +
    "            <img ng-src=\"{{car.imageName}}\" height=\"100\" width=\"200\">\n" +
    "        </p>\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeCar({carId:car.rid})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateCar({carId:car.rid})\" class=\"btn btn-warning\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>\n" +
    "");
}]);

angular.module("car/update_car.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("car/update_car.tpl.html",
    "<html ng-app=\"ngBoilerplate.account\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<body>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Car Information\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\"ng-control=\"UpdateAccountCtrl\">\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>Category:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.category\" class=\"form-control\" required/>\n" +
    "        <label>Brand:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.brand\" class=\"form-control\"required/>\n" +
    "        <label>Model:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.model\" class=\"form-control\"required/>\n" +
    "        <label>Doors:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.doors\" class=\"form-control\"required/>\n" +
    "        <label>Fuel Type:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.fuelType\" class=\"form-control\"required/>\n" +
    "        <label>Engine Size:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.engineSize\" class=\"form-control\"required/>\n" +
    "        <form name=\"Price\">\n" +
    "            <label>Price:</label>\n" +
    "            <input type=\"number\" name=\"price\" min=\"0\" class=\"form-control\" ng-model=\"car.price\" required/>\n" +
    "            <span style=\"color:red\" ng-show=\"Price.price.$dirty && Price.price.$invalid\">Price must be a positive value</span>\n" +
    "        </form>\n" +
    "        <label>Location:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.location\" class=\"form-control\" required/>\n" +
    "        <label>Image source:</label>\n" +
    "        <input type=\"text\" ng-model=\"car.imageName\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateCarInfo({category:car.category},{brand:car.brand},{model:car.model},{doors:car.doors},{fuelType:car.fuelType},{engineSize:car.engineSize},{price:car.price},{location:car.location},{imageName:car.imageName})\" class=\"btn btn-warning\"/>\n" +
    "</div>\n" +
    "</body>\n" +
    "</html>");
}]);

angular.module("clients/clients.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("clients/clients.tpl.html",
    "<html ng-app=\"ngBoilerplate.client\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Client\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div ng-control=\"ClientManageCtrl\">\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{client.rid}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>First Name: </th>\n" +
    "            <td>{{client.firstName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Last Name: </th>\n" +
    "            <td>{{client.lastName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                   <th>Age: </th>\n" +
    "            <td>{{client.age}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                    <th>Email: </th>\n" +
    "            <td>{{client.email}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "                    <th>CNP: </th>\n" +
    "                    <td>{{client.cnp}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                <span class=\"info\">\n" +
    "            <th>Phone number: </th>\n" +
    "            <td>{{client.phoneNumber}}</td>\n" +
    "                </span>\n" +
    "            </p>\n" +
    "\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeClient({clientId:client.rid})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateClient({clientId:client.rid})\" class=\"btn btn-default\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("clients/update_client.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("clients/update_client.tpl.html",
    "<html ng-app=\"ngBoilerplate.client\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<body>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Client Information\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\"ng-control=\"ClientManageCtrl\">\n" +
    "<div class=\"form-group\"  >\n" +
    "    <label>First name:</label>\n" +
    "    <input type=\"text\" ng-model=\"client.firstName\" class=\"form-control\" required/>\n" +
    "    <label>Last name:</label>\n" +
    "    <input type=\"text\" ng-model=\"client.lastName\" class=\"form-control\"required/>\n" +
    "    <form name=\"Age\">\n" +
    "        <label>Age:</label>\n" +
    "        <input type=\"number\" name=\"age\" ng-model=\"client.age\" min=\"18\" max=\"99\" class=\"form-control\"required/>\n" +
    "        <span style=\"color:red\" ng-show=\"Age.age.$dirty && Age.age.$invalid\">Age must be at least 18!</span>\n" +
    "    </form>\n" +
    "    <form name=\"Email\">\n" +
    "        <label>Email:</label>\n" +
    "        <input type=\"email\" name=\"email\"ng-model=\"client.email\" class=\"form-control\"  required/>\n" +
    "        <span style=\"color:red\" ng-show=\"Email.email.$dirty && Email.email.$invalid\">Not a valid e-mail!</span>\n" +
    "    </form>\n" +
    "    <form name=\"CNP\">\n" +
    "        <label>CNP:</label>\n" +
    "        <input  type=\"text\" style=\"width:130px;\"name=\"cnp\" minlength=\"13\" maxlength=\"13\" ng-pattern=\"/^[1-6]{1}[0-9]{12}$/\" title=\"The CNP must be 13 characters.\" ng-model=\"client.cnp\" class=\"form-control\" required/>\n" +
    "        <span style=\"color:red\" ng-show=\"CNP.cnp.$dirty && CNP.cnp.$invalid\" >Not a CNP!</span>\n" +
    "    </form>\n" +
    "    <form name=\"PhoneNumber\">\n" +
    "        <label>Phone number:</label>\n" +
    "        <input type=\"text\" style=\"width:130px;\" name=\"PhoneNumber\" minlength=\"10\" maxlength=\"10\" ng-pattern=\"/^[0]{1}[7]{1}[0-9]{8}$/\" title=\"Enter a phone number.\" ng-model=\"client.phoneNumber\" class=\"form-control\" required/>\n" +
    "        <span style=\"color:red\" ng-show=\"PhoneNumber.phoneNumber.$dirty && phoneNumber.phoneNumber.$invalid\">Only romanian phone numbers are valid.</span>\n" +
    "    </form>\n" +
    "</div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateClientInfo({firstName: client.firstName},{lastName: client.lastName},{age: client.age},{email: client.email},{cnp:client.cnp},{phoneNumber:client.phoneNumber})\" class=\"btn btn-default\"/>\n" +
    "</div>\n" +
    "</body>\n" +
    "</html>");
}]);

angular.module("employee/employee.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("employee/employee.tpl.html",
    "<html ng-app=\"ngBoilerplate.employee\"  ng-controller=\"EmployeeCtrl\">\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Employee\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{employee.rid}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>First Name: </th>\n" +
    "            <td>{{employee.firstName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Last Name: </th>\n" +
    "            <td>{{employee.lastName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeEmployee({employeeId:employee.rid})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateEmployee({employeeId:employee.rid})\" class=\"btn btn-warning\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>\n" +
    "");
}]);

angular.module("employee/update_employee.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("employee/update_employee.tpl.html",
    "<html ng-app=\"ngBoilerplate.employee\"  >\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Clients\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"employeeSearch\">\n" +
    "                        <i class=\"fa fa-users\"></i>\n" +
    "                        Search Employees\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"carSearch\">\n" +
    "                        <i class=\"fa fa-car\"></i>\n" +
    "                        Search Cars\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\"ng-control=\"UpdateEmployeeCtrl\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Update Employee\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>First name:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.firstName\" class=\"form-control\" required/>\n" +
    "        <label>Last name:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.lastName\" class=\"form-control\"required/>\n" +
    "       <label>Username:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.username\" class=\"form-control\"required/>\n" +
    "        <label>Password:</label>\n" +
    "        <input type=\"password\" ng-model=\"employee.password\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateEmployeeInfo({firstName: employee.firstName},{lastName: employee.lastName},{username:employee.username},{password:employee.password})\" class=\"btn btn-default\"/>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("employee_home/employee_home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("employee_home/employee_home.tpl.html",
    "<html ng-app=\"ngBoilerplate.employee_home\"  ng-controller=\"EmployeeHomeCtrl\">\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"employee_home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h2 class=\"page-header\">\n" +
    "        Return car\n" +
    "    </h2>\n" +
    "</div>\n" +
    "<div class=\"form-group\">\n" +
    "    <label for=\"repeatCar\">Car:</label>\n" +
    "    <select id=\"repeatCar\" ng-model=\"dataCar.repeatCar\">\n" +
    "        <option ng-repeat=\"car in dataCar.availableCar\" ng-show=\"carFilter(car)\" value=\"{{car.rid}}\">{{car.rid}}</option>\n" +
    "    </select>\n" +
    "</div>\n" +
    "<input type=\"submit\" value=\"Return\" ng-click=\"returnCar()\" class=\"btn btn-primary\"/>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Renting Details\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"q\" placeholder=\"Car info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>Client</th>\n" +
    "        <th>Car Brand</th>\n" +
    "        <th>Car Model</th>\n" +
    "        <th>Pick up location</th>\n" +
    "        <th>Return location</th>\n" +
    "        <th>Pick up date</th>\n" +
    "        <th>Drop off date</th>\n" +
    "        <th>Pick up hour</th>\n" +
    "        <th>Drop off hour</th>\n" +
    "        <tr ng-repeat=\"rentingDetail in rentingDetails|filter:q&&myFilter\">\n" +
    "            <td>{{rentingDetail.client.firstName}} {{rentingDetail.client.lastName}}</td>\n" +
    "            <td>{{rentingDetail.car.brand}}</td>\n" +
    "            <td>{{rentingDetail.car.model}}</td>\n" +
    "            <td>{{rentingDetail.pickUpLocation}}</td>\n" +
    "            <td>{{rentingDetail.returnLocation}}</td>\n" +
    "            <td>{{rentingDetail.pickUpDate}}</td>\n" +
    "            <td>{{rentingDetail.dropOffDate}}</td>\n" +
    "            <td>{{rentingDetail.pickUpHour}}</td>\n" +
    "            <td>{{rentingDetail.dropOffHour}}</td>\n" +
    "            <td>\n" +
    "                <input type=\"submit\" value=\"Create contract\" data-ng-click=\"createContract({rentingDetailId:rentingDetail.rid})\" class=\"btn btn-default\"/>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "\n" +
    "    </table>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<html ng-app=\"ngBoilerplate.home\"  ng-controller=\"HomeCtrl\">\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Cars\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"q\" placeholder=\"Car info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>Car</th>\n" +
    "        <th>Brand</th>\n" +
    "        <th>Model</th>\n" +
    "        <th>Category</th>\n" +
    "        <th>Doors</th>\n" +
    "        <th>Fuel Type</th>\n" +
    "        <th>Engine</th>\n" +
    "        <th>Price</th>\n" +
    "        <th>Location</th>\n" +
    "        <tr ng-repeat=\"car in cars|filter: q\" ng-show=\"myFilter(car)\">\n" +
    "            <td><img ng-src=\"{{car.imageName}}\" width=\"200\" height=\"100\"></td>\n" +
    "            <td>{{car.brand}}</td>\n" +
    "            <td>{{car.model}}</td>\n" +
    "            <td>{{car.category}}</td>\n" +
    "            <td>{{car.doors}}</td>\n" +
    "            <td>{{car.fuelType}}</td>\n" +
    "            <td>{{car.engineSize}}</td>\n" +
    "            <td>{{car.price}} &#8364</td>\n" +
    "            <td>{{car.location}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"rent({carId:car.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Rent\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "\n" +
    "    </table>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("home/rentCar.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/rentCar.tpl.html",
    "<html ng-app=\"ngBoilerplate.home\" ng-controller=\"RentCtrl\" >\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Enter information\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "            <div class=\"form-group\"  >\n" +
    "                <label>First name:</label>\n" +
    "                <input type=\"text\" ng-model=\"firstName\" class=\"form-control\" required/>\n" +
    "                <label>Last name:</label>\n" +
    "                <input type=\"text\" ng-model=\"lastName\" class=\"form-control\"required/>\n" +
    "                <form name=\"Age\">\n" +
    "                    <label>Age:</label>\n" +
    "                    <input type=\"number\" name=\"age\" ng-model=\"age\" min=\"18\" max=\"99\" class=\"form-control\"required/>\n" +
    "                    <span style=\"color:red;\" ng-show=\"Age.age.$dirty && Age.age.$invalid\">Age must be at least 18!</span>\n" +
    "                </form>\n" +
    "                <form name=\"Email\">\n" +
    "                    <label>Email:</label>\n" +
    "                    <input type=\"email\" name=\"email\"ng-model=\"email\" class=\"form-control\"  required/>\n" +
    "                    <span style=\"color:red;\" ng-show=\"Email.email.$dirty && Email.email.$invalid\">Not a valid e-mail!</span>\n" +
    "                </form>\n" +
    "                <form name=\"CNP\">\n" +
    "                    <label>CNP:</label>\n" +
    "                    <input  type=\"text\" style=\"width:130px;\"name=\"cnp\" minlength=\"13\" maxlength=\"13\" ng-pattern=\"/^[1-6]{1}[0-9]{12}$/\" title=\"The CNP must be 13 characters.\" ng-model=\"cnp\" class=\"form-control\" required/>\n" +
    "                    <span style=\"color:red;\" ng-show=\"CNP.cnp.$dirty && CNP.cnp.$invalid\" >Not a CNP!</span>\n" +
    "                </form>\n" +
    "                <form name=\"PhoneNumber\">\n" +
    "                    <label>Phone number:</label>\n" +
    "                    <input type=\"text\" style=\"width:130px;\" name=\"phoneNumber\" minlength=\"10\" maxlength=\"10\" ng-pattern=\"/^[0]{1}[7]{1}[0-9]{8}$/\" title=\"Enter a phone number.\" ng-model=\"phoneNumber\" class=\"form-control\" required/>\n" +
    "                    <span style=\"color:red;\" ng-show=\"PhoneNumber.phoneNumber.$dirty && PhoneNumber.phoneNumber.$invalid\">Only romanian phone numbers are valid.</span>\n" +
    "                </form>\n" +
    "                <form name=\"PickUpHour\">\n" +
    "                    <label>Pick up hour</label>\n" +
    "                    <input type=\"number\" name=\"pickUpHour\" ng-model=\"pickUpHour\" style=\"width:80px;\" min=\"7\" max=\"23\" title=\"Cars can be picked up between 7 AM and 11 PM\" class=\"form-control\" required/>\n" +
    "                    <span style=\"color:red;\" ng-show=\"PickUpHour.pickUpHour.$dirty && PickUpHour.pickUpHour.$invalid\">Cars can be picked up between 7 AM and 11 PM</span>\n" +
    "                </form>\n" +
    "                <label>Pickup date:</label>\n" +
    "                <input type=\"date\"style=\"width:155px;\" ng-model=\"pickUpDate\" min=\"2016-05-11\" class=\"form-control\"required/>\n" +
    "                <form name=\"DropOffHour\">\n" +
    "                    <label>Drop off hour</label>\n" +
    "                    <input type=\"number\" name=\"dropOffHour\" ng-model=\"dropOffHour\" style=\"width:80px;\" min=\"7\" max=\"23\" title=\"Cars can be returned between 7 AM and 11 PM\" class=\"form-control\" required/>\n" +
    "                    <span style=\"color:red;\" ng-show=\"DropOffHour.dropOffHour.$dirty && DropOffHour.dropOffHour.$invalid\">Cars can be returned between 7 AM and 11 PM</span>\n" +
    "                </form>\n" +
    "                <label>Drop off date:</label>\n" +
    "                <input type=\"date\"style=\"width:155px;\" ng-model=\"dropOffDate\" min=\"2016-05-11\" class=\"form-control\"required/>\n" +
    "                <label>Return Location:</label>\n" +
    "                <select ng-model=\"returnLocation\" ng-options=\"x for x in cities\">\n" +
    "                </select>\n" +
    "            </div>\n" +
    "    <input type=\"submit\" value=\"Submit\" ng-click=\"addRentingDetails(firstName,lastName,age,email,CNP,phoneNumber,pickUpHour,pickUpDate,pickUpLocation,dropOffHour,dropOffDate,returnLocation)\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("login/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("login/login.tpl.html",
    "<html ng-app=\"ngBoilerplate.login\" ng-controller=\"LoginCtrl\">\n" +
    "<div class=\"row\">\n" +
    "  <h1 class=\"page-header\">\n" +
    "      Login\n" +
    "  </h1>\n" +
    "  <form ng-submit=\"login()\">\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Username:</label>\n" +
    "          <input type=\"text\" ng-model=\"user.username\" class=\"form-control\"/>\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Password:</label>\n" +
    "          <input type=\"password\" ng-model=\"user.password\" class=\"form-control\"/>\n" +
    "      </div>\n" +
    "      <button class=\"btn btn-success\" type=\"submit\">Login</button>\n" +
    "  </form>\n" +
    "</div>\n" +
    "\n" +
    "</html>");
}]);
