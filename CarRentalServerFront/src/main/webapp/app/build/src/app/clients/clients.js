angular.module( 'ngBoilerplate.client', [
  'ui.router',
  'plusOne',
  'ngBoilerplate.login'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'manageClient', {
    url: '/clients/client?clientId',
    views: {
      'main': {
        controller: 'ClientManageCtrl',
        templateUrl: 'clients/clients.tpl.html'
      }
    },
    data:{ pageTitle: 'Client' },
    resolve:{
        client: function(clientInfoService,$stateParams){
            return clientInfoService.getClientById($stateParams.clientId);
        }
    }
    })
    .state('updateClient',{
     url:'/clients/update/client?clientId',
     views:{
        'main':{
            controller: 'UpdateClientCtrl',
            templateUrl:'clients/update_client.tpl.html'
        }
     },
     data:{ pageTitle: 'Update Client Info'},
     resolve:{
        client: function(clientInfoService,$stateParams){
             return clientInfoService.getClientById($stateParams.clientId);
         }
    }
    });
})
.factory('clientInfoService',function($resource){
     var service={};
     service.getClientById = function(clientId) {
         var Client = $resource("/CarRental/clients/:paramClientId");
         return Client.get({paramClientId:clientId}).$promise;
     };
     service.removeClient=function(clientId){
        var Client=$resource("/CarRental/clients/:paramClientId");
        return Client.remove({paramClientId:clientId}).$promise;
     };
     service.updateClientInfo=function(clientId,updatedClient){
        var Client=$resource("/CarRental/clients/:paramClientId");
        return Client.save({paramClientId:clientId},updatedClient).$promise;
     };
     service.addClient=function(savedClient){
        var Client=$resource("/CarRental/clients");
        return Client.save(savedClient).$promise;
     };
     service.getAllClients=function(){
         var Client=$resource("/CarRental/clients");
         return Client.get().$promise.then(function(data){
             return data.clients;
         });
     };
     return service;
 })
 .controller('UpdateClientCtrl',function($scope,$state,clientInfoService,client,sessionService){
     $scope.isLoggedIn = sessionService.isLoggedIn;
     $scope.logout = sessionService.logout;
     $scope.client=client;
     $scope.updateClientInfo=function(firstName,lastName,age,email,CNP,phoneNumber){
        clientInfoService.updateClientInfo(client.rid,{
            firstName: client.firstName,
            lastName: client.lastName,
            email:client.email,
            age: client.age,
            cnp : client.cnp,
            phoneNumber: client.phoneNumber
        }).then(function(){
            $state.go("manageClient",{clientId:client.rid},{reload:true});
        });

     };
  })
.controller('ClientManageCtrl',function($scope,$state,clientInfoService,client,sessionService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
     $scope.client=client;
     $scope.removeClient=function(){
             clientInfoService.removeClient(client.rid).then(function(){
                 $state.go("admin_home",{reload:true});
             });
             };
    });