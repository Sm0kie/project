package core.security;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import rest.resources.EmployeeResource;
import rest.resources.EmployeeResourceList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Chris on 10/19/14.
 */
@Component
public class EmployeeDetailServiceImpl implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        EmployeeResource employee;
        EmployeeResourceList employees;
        try {
            employees=findEmployeeByUsername(name,null);
            employee=employees.getEmployees().get(0);
            if(employee == null) {
                throw new UsernameNotFoundException("no user found with " + name);
            }
            return new EmployeeDetails(employee);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private EmployeeResourceList findEmployeeByUsername(String username,String password)throws IOException{
        EmployeeResourceList employees;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/employees")
                .setParameter("username", username)
                .setParameter("password", password);
        URI uri = null;
        try {
            uri = builder.build();
        } catch (URISyntaxException e) {
        }
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        employees = new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())), EmployeeResourceList.class);
        return employees;
    }
}
