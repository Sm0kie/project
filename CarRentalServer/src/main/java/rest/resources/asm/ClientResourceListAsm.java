package rest.resources.asm;

import core.models.entities.Client;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.ClientController;
import rest.resources.ClientResource;
import rest.resources.ClientResourceList;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public class ClientResourceListAsm extends ResourceAssemblerSupport<List<Client>,ClientResourceList> {
    public ClientResourceListAsm(){
        super(ClientController.class,ClientResourceList.class);
    }
    @Override
    public ClientResourceList toResource(List<Client> clients) {
        List<ClientResource> resList=new ClientResourceAsm().toResources(clients);
        ClientResourceList finalRes = new ClientResourceList();
        finalRes.setClients(resList);
        return finalRes;
    }
}