package rest.resources.asm;

import core.models.Car;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.CarController;
import rest.resources.CarResource;

/**
 * Created by ilyes on 5/8/2016.
 */
public class CarResourceAsm extends ResourceAssemblerSupport<Car,CarResource> {
    public CarResourceAsm(){
        super(CarController.class,CarResource.class);
    }
    @Override
    public CarResource toResource(Car car) {
        CarResource res = new CarResource();
        res.setRid(car.getId());
        res.setModel(car.getModel());
        res.setAvailable(car.getAvailable());
        res.setBrand(car.getBrand());
        res.setCategory(car.getCategory());
        res.setDoors(car.getDoors());
        res.setEngineSize(car.getEngineSize());
        res.setFuelType(car.getFuelType());
        res.setImageName(car.getImageName());
        res.setPrice(car.getPrice());
        res.setLocation(car.getLocation());
        return res;
    }
}
