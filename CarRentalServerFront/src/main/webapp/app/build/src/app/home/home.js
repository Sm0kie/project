angular.module( 'ngBoilerplate.home', [
  'ui.router',
  'ngBoilerplate.car',
  'ngBoilerplate.employee_home',
  'ngBoilerplate.client',
  'ngResource'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      'main': {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    resolve:{
        cars: function(carInfoService) {
            return carInfoService.getAllCars();
        }
    },
    data:{ pageTitle: 'Home' }
})
.state('rent',{
        url:'/home/rent/car?carId',
        views:{
            'main':{
                templateUrl:'home/rentCar.tpl.html',
                controller:'RentCtrl'
            }
        },
        data:{pageTitle:'Rent a Car'},
        resolve:{
            car: function(carInfoService,$stateParams){
                return carInfoService.getCarById($stateParams.carId);
            }
        }
});
})
.controller('RentCtrl',function($scope, $state,carInfoService,car,rentingDetailsInfoService,clientInfoService){
    $scope.cities=["Cluj-Napoca","Alba Iulia","Timisoara","Bucuresti"];
    $scope.addRentingDetails=function(firstName,lastName,age,email,cnp,phoneNumber,pickUpHour,pickUpDate,pickUpLocation,dropOffHour,dropOffDate,returnLocation){
        rentingDetailsInfoService.addRentingDetails(car.rid,{
        client:{
            firstName: $scope.firstName,
            lastName: $scope.lastName,
            age: $scope.age,
            email:$scope.email,
            cnp:$scope.cnp,
            phoneNumber:$scope.phoneNumber
        },rentingDetails:{
            pickUpHour: $scope.pickUpHour,
            pickUpDate: $scope.pickUpDate,
            pickUpLocation:$scope.pickUpLocation,
            dropOffHour: $scope.dropOffHour,
            dropOffDate: $scope.dropOffDate,
            returnLocation: $scope.returnLocation
        }}).then(function(){
                    $state.go("home",{reload:true});
                    //! TODO GET client POST renting Details somehow
        });
    };
})
.controller( 'HomeCtrl', function HomeController(cars,$scope) {
    $scope.cars=cars;
    $scope.myFilter=function(car){
        if(car.available===false){
            return false;
        }
        return true;
    };
});

