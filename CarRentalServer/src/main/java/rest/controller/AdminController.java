package rest.controller;

import core.dao.AdminDao;
import core.models.entities.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.resources.AdminResource;
import rest.resources.AdminResourceList;
import rest.resources.asm.AdminResourceAsm;
import rest.resources.asm.AdminResourceListAsm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
@RequestMapping(value="/admins")
public class AdminController {
    @Autowired
    private AdminDao adminDao;

    @RequestMapping(method= RequestMethod.GET)
    public ResponseEntity<AdminResourceList> findAllAdmins(
            @RequestParam(value="username",required=false) String username,
            @RequestParam(value="password",required=false) String password){
        List<Admin> admins;
        Admin admin;
        AdminResourceList adm;
        if(username==null){
            admins=adminDao.findAllAdmins();
        }else{
            admin=adminDao.findAdminByUserName(username);
            admins=new ArrayList<Admin>();
            if(admin!=null){
                if(password!=null){
                    if(admin.getPassword().equals(password)){
                        admins=new ArrayList<Admin>(Arrays.asList(admin));
                    }else{
                        admins = new ArrayList<Admin>(Arrays.asList(admin));
                    }
                }else{
                    admins=new ArrayList<Admin>(Arrays.asList(admin));
                }
            }
        }
        adm=new AdminResourceListAsm().toResource(admins);
        if(admins.isEmpty())
            return new ResponseEntity<AdminResourceList>(adm, HttpStatus.NOT_FOUND);
        return new ResponseEntity<AdminResourceList>(adm,HttpStatus.OK);
    }

    @RequestMapping(value="/{adminId}",method=RequestMethod.GET)
    public ResponseEntity<AdminResource> findAdminById(
            @PathVariable Long adminId){
        Admin admin=adminDao.findAdminById(adminId);
        AdminResource res;
        if(admin==null){
            admin=new Admin();
            admin.setId(0l);
            admin.setFirstName("0");
            admin.setLastName("0");
            admin.setUsername("0");
            admin.setPassword("0");
            res=new AdminResourceAsm().toResource(admin);
            return new ResponseEntity<AdminResource>(res,HttpStatus.NOT_FOUND);
        }
        res=new AdminResourceAsm().toResource(admin);
        return new ResponseEntity<AdminResource>(res,HttpStatus.OK);
    }
}
