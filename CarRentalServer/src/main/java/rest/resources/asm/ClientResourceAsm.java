package rest.resources.asm;

import core.models.entities.Client;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.ClientController;
import rest.resources.ClientResource;

/**
 * Created by ilyes on 5/8/2016.
 */
public class ClientResourceAsm extends ResourceAssemblerSupport<Client,ClientResource> {
    public ClientResourceAsm(){
        super(ClientController.class,ClientResource.class);
    }
    @Override
    public ClientResource toResource(Client client) {
        ClientResource res=new ClientResource();
        res.setRid(client.getId());
        res.setCNP(client.getCNP());
        res.setFirstName(client.getFirstName());
        res.setLastName(client.getLastName());
        res.setAge(client.getAge());
        res.setEmail(client.getEmail());
        res.setPhoneNumber(client.getPhoneNumber());
        return res;
    }
}
