package core.dao;

import core.models.entities.Admin;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public interface AdminDao {
    List<Admin> findAllAdmins();
    Admin findAdminByUserName(String username);
    Admin findAdminById(Long id);
}
