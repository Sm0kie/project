package rest.resources;

import core.models.entities.Car;
import core.models.entities.Client;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ilyes on 5/8/2016.
 */
public class RentingDetailsResource extends ResourceSupport {
    private Long rid;
    private String pickUpLocation;
    private String returnLocation;
    private String pickUpDate;
    private String dropOffDate;
    private String pickUpHour;
    private String dropOffHour;
    private Car car;
    private Client client;

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getReturnLocation() {
        return returnLocation;
    }

    public void setReturnLocation(String returnLocation) {
        this.returnLocation = returnLocation;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getDropOffDate() {
        return dropOffDate;
    }

    public void setDropOffDate(String dropOffDate) {
        this.dropOffDate = dropOffDate;
    }

    public String getPickUpHour() {
        return pickUpHour;
    }

    public void setPickUpHour(String pickUpHour) {
        this.pickUpHour = pickUpHour;
    }

    public String getDropOffHour() {
        return dropOffHour;
    }

    public void setDropOffHour(String dropOffHour) {
        this.dropOffHour = dropOffHour;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
