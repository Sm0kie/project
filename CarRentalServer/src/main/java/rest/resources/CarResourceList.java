package rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public class CarResourceList extends ResourceSupport {
    private List<CarResource> cars=new ArrayList<CarResource>();

    public List<CarResource> getCars() {
        return cars;
    }

    public void setCars(List<CarResource> cars) {
        this.cars = cars;
    }
}
