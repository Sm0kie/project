package rest.resources.asm;

import core.models.Admin;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.AdminController;
import rest.resources.AdminResource;
import rest.resources.AdminResourceList;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public class AdminResourceListAsm extends ResourceAssemblerSupport<List<Admin>,AdminResourceList>{
    public AdminResourceListAsm(){
        super(AdminController.class,AdminResourceList.class);
    }
    @Override
    public AdminResourceList toResource(List<Admin> admins) {
        List<AdminResource> resList = new AdminResourceAsm().toResources(admins);
        AdminResourceList finalRes = new AdminResourceList();
        finalRes.setAdmins(resList);
        return finalRes;
    }
}
