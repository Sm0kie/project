package rest.resources;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ilyes on 5/8/2016.
 */
public class AdminResource extends ResourceSupport {
    private Long rid;
    private String firstName;
    private String lastName;
    private String username;
    private String password;

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
