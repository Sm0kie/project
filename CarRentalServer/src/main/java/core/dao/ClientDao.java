package core.dao;

import core.models.entities.Client;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public interface ClientDao {
    List<Client> findAllClients();
    Client findClientById(Long id);
    List<Client> findClientByCNP(String CNP);
    Client createClient(Client data);
    Client updateClient(Long clientId,Client data);
    Client removeClient(Long id);
}
