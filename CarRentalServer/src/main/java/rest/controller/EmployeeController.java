package rest.controller;

import core.dao.EmployeeDao;
import core.models.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.EmployeeResource;
import rest.resources.EmployeeResourceList;
import rest.resources.asm.EmployeeResourceAsm;
import rest.resources.asm.EmployeeResourceListAsm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {
    @Autowired
    private EmployeeDao employeeDao;

    @RequestMapping(method= RequestMethod.GET)
    public ResponseEntity<EmployeeResourceList> findAllEmployees(
            @RequestParam(value="username",required=false) String username,
            @RequestParam(value="password",required=false) String password){
        List<Employee> employeeList;
        Employee employee;
        EmployeeResourceList res;
        if(username==null)
            employeeList=employeeDao.findAllEmployees();
        else{
            employee=employeeDao.findEmployeeByUserName(username);
            employeeList=new ArrayList<Employee>();
            if(employee!=null){
                if(password!=null){
                    if(employee.getPassword().equals(password))
                        employeeList=new ArrayList<Employee>(Arrays.asList(employee));
                    else
                        employeeList=new ArrayList<Employee>(Arrays.asList(employee));
                }else
                    employeeList=new ArrayList<Employee>(Arrays.asList(employee));
            }
        }
        res=new EmployeeResourceListAsm().toResource(employeeList);
        if(employeeList.isEmpty())
            return new ResponseEntity<EmployeeResourceList>(res, HttpStatus.NOT_FOUND);
        return new ResponseEntity<EmployeeResourceList>(res,HttpStatus.OK);
    }

    @RequestMapping(value="/{employeeId}",method=RequestMethod.GET)
    public ResponseEntity<EmployeeResource> findEmployeeById(
            @PathVariable Long employeeId){
        Employee employee=employeeDao.findEmployeeById(employeeId);
        EmployeeResource res;
        if(employee==null){
            employee=new Employee();
            employee.setId(0l);
            employee.setUsername("0");
            employee.setPassword("0");
            employee.setFirstName("0");
            employee.setLastName("0");
            res=new EmployeeResourceAsm().toResource(employee);
            return new ResponseEntity<EmployeeResource>(res,HttpStatus.NOT_FOUND);
        }
        res=new EmployeeResourceAsm().toResource(employee);
        return new ResponseEntity<EmployeeResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value="/{employeeId}",method=RequestMethod.DELETE)
    public ResponseEntity<EmployeeResource> removeEmployee(
            @PathVariable Long employeeId){
        Employee employee = employeeDao.removeEmployee(employeeId);
        if(employee==null)
            return new ResponseEntity<EmployeeResource>(HttpStatus.NOT_FOUND);
        EmployeeResource res = new EmployeeResourceAsm().toResource(employee);
        return new ResponseEntity<EmployeeResource>(res,HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<EmployeeResource> addEmployee(
            @RequestBody Employee data,
            UriComponentsBuilder ucBuilder){
        EmployeeResource res;
        Employee employee=employeeDao.findEmployeeByUserName(data.getUsername());
        if(employee!=null){
            employee=new Employee();
            employee.setId(0l);
            employee.setUsername("0");
            employee.setPassword("0");
            employee.setFirstName("0");
            employee.setLastName("0");
            res=new EmployeeResourceAsm().toResource(employee);
            return new ResponseEntity<EmployeeResource>(res,HttpStatus.CONFLICT);
        }
        employee=employeeDao.createEmployee(data);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/employees/{employeeId}").buildAndExpand(employee.getId()).toUri());
        res = new EmployeeResourceAsm().toResource(employee);
        return new ResponseEntity<EmployeeResource>(res,headers,HttpStatus.CREATED);
    }

    @RequestMapping(value="/{employeeId}",method=RequestMethod.POST)
    public ResponseEntity<EmployeeResource> updateEmployee(
            @PathVariable Long employeeId,
            @RequestBody Employee data){
        EmployeeResource res;
        Employee employee=employeeDao.updateEmployee(employeeId,data);
        if(employee==null){
            employee=new Employee();
            employee.setId(0l);
            employee.setUsername("0");
            employee.setPassword("0");
            employee.setFirstName("0");
            employee.setLastName("0");
            res=new EmployeeResourceAsm().toResource(employee);
            return new ResponseEntity<EmployeeResource>(res,HttpStatus.NOT_FOUND);
        }
        res=new EmployeeResourceAsm().toResource(employee);
        return new ResponseEntity<EmployeeResource>(res,HttpStatus.OK);
    }
}
