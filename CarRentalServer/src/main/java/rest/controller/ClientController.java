package rest.controller;

import core.dao.ClientDao;
import core.dao.RentingDetailsDao;
import core.models.entities.Client;
import core.models.entities.RentingDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.ClientResource;
import rest.resources.ClientResourceList;
import rest.resources.asm.ClientResourceAsm;
import rest.resources.asm.ClientResourceListAsm;

import java.util.Iterator;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
@RequestMapping(value="/clients")
public class ClientController {
    @Autowired
    private RentingDetailsDao rentingDetailsDao;
    @Autowired
    private ClientDao clientDao;
    @RequestMapping(method= RequestMethod.GET)
    public ResponseEntity<ClientResourceList> findAllClients(){
        ClientResourceList clients=new ClientResourceListAsm().toResource(clientDao.findAllClients());
        if(clients.getClients().isEmpty())
            return new ResponseEntity<ClientResourceList>(clients, HttpStatus.NOT_FOUND);
        return new ResponseEntity<ClientResourceList>(clients,HttpStatus.OK);
    }

    @RequestMapping(value="/{clientId}",method=RequestMethod.GET)
    public ResponseEntity<ClientResource> findClientById(
            @PathVariable Long clientId){
        Client client =clientDao.findClientById(clientId);
        ClientResource res;
        if(client==null){
            client=new Client();
            client.setId(0l);
            client.setEmail("0");
            client.setPhoneNumber("0");
            client.setAge(0l);
            client.setLastName("0");
            client.setFirstName("0");
            client.setCNP("0");
            res=new ClientResourceAsm().toResource(client);
            return new ResponseEntity<ClientResource>(res,HttpStatus.NOT_FOUND);
        }
        res=new ClientResourceAsm().toResource(client);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value="/{clientId}",method=RequestMethod.DELETE)
    public ResponseEntity<ClientResource> removeCar(
            @PathVariable Long clientId){
        List<RentingDetails> rentingDetails=rentingDetailsDao.findRentingDetailsByClient(clientId);
        Iterator<RentingDetails> it= rentingDetails.iterator();
        if(rentingDetails!=null){
            while(it.hasNext()){
                RentingDetails rentingDet=it.next();
                rentingDetailsDao.removeRentingDetails(rentingDet.getId());
            }
        }
        Client client=clientDao.removeClient(clientId);
        if(client==null)
            return new ResponseEntity<ClientResource>(HttpStatus.NOT_FOUND);
        ClientResource res = new ClientResourceAsm().toResource(client);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<ClientResource> addClient(
            @RequestBody Client data,
            UriComponentsBuilder ucBuilder){
        ClientResource res;
        List<Client> clients=clientDao.findClientByCNP(data.getCNP());
        Client client;
        if(clients.isEmpty()) {
            client = clientDao.createClient(data);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/clients/{clientId}").buildAndExpand(client.getId()).toUri());
            res = new ClientResourceAsm().toResource(client);
            return new ResponseEntity<ClientResource>(res, headers, HttpStatus.CREATED);
        }
        res=new ClientResourceAsm().toResource(clients.get(0));
        return new ResponseEntity<ClientResource>(res,HttpStatus.CONFLICT);
    }

    @RequestMapping(value="/{clientId}",method=RequestMethod.POST)
    public ResponseEntity<ClientResource> updateClient(
            @PathVariable Long clientId,
            @RequestBody Client data){
        ClientResource res;
        Client client = clientDao.updateClient(clientId,data);
        if(client==null){
            client=new Client();
            client.setId(0l);
            client.setEmail("0");
            client.setPhoneNumber("0");
            client.setAge(0l);
            client.setLastName("0");
            client.setFirstName("0");
            client.setCNP("0");
            res=new ClientResourceAsm().toResource(client);
            return new ResponseEntity<ClientResource>(res,HttpStatus.NOT_FOUND);
        }
        res = new ClientResourceAsm().toResource(client);
        return new ResponseEntity<ClientResource>(res,HttpStatus.OK);
    }
}
