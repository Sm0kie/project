package core.dao;

import core.models.entities.RentingDetails;

import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public interface RentingDetailsDao {
    List<RentingDetails> findAllRentingDetails();
    RentingDetails findRentingDetailsById(Long id);
    List<RentingDetails> findRentingDetailsByClient(Long id);
    List<RentingDetails> findRentingDetailsByCar(Long id);
    RentingDetails createRentingDetails(RentingDetails data);
    RentingDetails updateRentingDetails(Long rentingDetailsId,RentingDetails data);
    RentingDetails removeRentingDetails(Long id);
}
