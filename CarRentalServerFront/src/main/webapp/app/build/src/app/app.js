angular.module( 'ngBoilerplate', [
  'templates-app',
  'templates-common',
  'ngBoilerplate.home',
  'ngBoilerplate.client',
  'ngBoilerplate.login',
  'ngBoilerplate.car',
  'ngBoilerplate.admin',
  'ngBoilerplate.employee',
  'ngBoilerplate.employee_home',
  'ui.router',
  'hateoas'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider, HateoasInterceptorProvider) {
  $urlRouterProvider
  .otherwise('/home');
  HateoasInterceptorProvider.transformAllResponses();
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $resource,$scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle )) {
      $scope.pageTitle = toState.data.pageTitle ;
    }
  });
});

