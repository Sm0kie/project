package rest.resources.asm;

import core.models.entities.RentingDetails;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.RentingDetailsController;
import rest.resources.RentingDetailsResource;

/**
 * Created by ilyes on 5/8/2016.
 */
public class RentingDetailsResourceAsm extends ResourceAssemblerSupport<RentingDetails,RentingDetailsResource> {
    public RentingDetailsResourceAsm(){
        super(RentingDetailsController.class,RentingDetailsResource.class);
    }
    @Override
    public RentingDetailsResource toResource(RentingDetails rentingDetails) {
        RentingDetailsResource res=new RentingDetailsResource();
        res.setRid(rentingDetails.getId());
        res.setPickUpDate(rentingDetails.getPickUpDate());
        res.setReturnLocation(rentingDetails.getReturnLocation());
        res.setCar(rentingDetails.getCar());
        res.setClient(rentingDetails.getClient());
        res.setDropOffDate(rentingDetails.getDropOffDate());
        res.setPickUpHour(rentingDetails.getPickUpHour());
        res.setPickUpLocation(rentingDetails.getPickUpLocation());
        res.setDropOffHour(rentingDetails.getDropOffHour());
        return res;
    }
}
