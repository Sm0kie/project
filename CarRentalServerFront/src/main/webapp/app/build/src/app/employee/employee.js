angular.module( 'ngBoilerplate.employee', [
  'ui.router',
  'plusOne',
  'ngBoilerplate.login',
  'ngBoilerplate.admin'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'manageEmployee', {
    url: '/employees/employee?employeeId',
    views: {
      'main': {
        controller: 'EmployeeManageCtrl',
        templateUrl: 'employee/employee.tpl.html'
      }
    },
    data:{ pageTitle: 'Employee' },
    resolve:{
        employee: function(employeeInfoService,$stateParams){
            return employeeInfoService.getEmployeeById($stateParams.employeeId);
        }
        }
    })
    .state('updateEmployee',{
     url:'/employees/update/employee?employeeId',
     views:{
        'main':{
            controller: 'UpdateEmployeeCtrl',
            templateUrl:'employee/update_employee.tpl.html'
        }
     },
     data:{ pageTitle: 'Update Employee Info'},
     resolve:{
        employee: function(employeeInfoService,$stateParams){
             return employeeInfoService.getEmployeeById($stateParams.employeeId);
         }
    }
    });
})
.factory('employeeInfoService',function($resource){
    var service={};
    service.getEmployeeById = function(employeeId) {
        var Employee= $resource("/CarRental/employees/:paramEmployeeId");
        return Employee.get({paramEmployeeId:employeeId}).$promise;
    };
    service.getAllEmployees=function(){
        var Employee=$resource("/CarRental/employees");
        return Employee.get().$promise.then(function(data){
            return data.employees;
        });
    };
    service.addEmployee=function(savedEmployee){
        var Employee=$resource("/CarRental/employees/");
        return Employee.save(savedEmployee).$promise;
     };
     service.updateEmployeeInfo=function(employeeId,updatedEmployee){
        var Employee=$resource("/CarRental/employees/:paramEmployeeId");
        return Employee.save({paramEmployeeId:employeeId},updatedEmployee).$promise;
     };
      service.removeEmployee=function(employeeId){
         var Employee=$resource("/CarRental/employees/:paramEmployeeId");
         return Employee.remove({paramEmployeeId:employeeId}).$promise;
      };
    return service;
})
.controller('UpdateEmployeeCtrl',function($scope,$state,employeeInfoService,employee,sessionService){
     $scope.isLoggedIn = sessionService.isLoggedIn;
     $scope.logout = sessionService.logout;
     $scope.employee=employee;
     $scope.updateEmployeeInfo=function(firstName,lastName,email,username,password){
        employeeInfoService.updateEmployeeInfo(employee.rid,{
           firstName: employee.firstName,
           lastName: employee.lastName,
           username: employee.username,
           password: employee.password
      }).then(function(){
          $state.go("manageEmployee",{employeeId:employee.rid},{reload:true});
      });
   };
})
.controller('EmployeeManageCtrl',function($scope,$state,employeeInfoService,employee,sessionService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
     $scope.employee=employee;
     $scope.removeEmployee=function(){
             employeeInfoService.removeEmployee(employee.rid).then(function(){
                 $state.go("employeeSearch",{reload:true});
             });
             };
 });