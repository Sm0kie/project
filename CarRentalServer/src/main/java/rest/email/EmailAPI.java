package rest.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * Created by ilyes on 5/12/2016.
 */
@Service("Email")
public class EmailAPI {
    @Autowired
    private MailSender mail;

    public void readyToSendEmail(String toAddress, String fromAddress,String subject, String msgBody){
        SimpleMailMessage message=new SimpleMailMessage();
        message.setFrom(fromAddress);
        message.setTo(toAddress);
        message.setSubject(subject);
        message.setText(msgBody);
        mail.send(message);
    }
}
