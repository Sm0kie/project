package rest.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rest.resources.AdminResource;
import rest.resources.AdminResourceList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
@RequestMapping(value="/admins")
public class AdminController {


    @RequestMapping(method= RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<AdminResourceList> findAllAdmins(
            @RequestParam(value="username",required=false) String username,
            @RequestParam(value="password",required=false) String password) throws IOException {
        AdminResourceList admins;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        if(username==null)
            builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/admins");
        else
            builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/admins")
                    .setParameter("username",username)
                    .setParameter("password",password);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        admins=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),AdminResourceList.class);
        if(admins.getAdmins().isEmpty())
            return new ResponseEntity<AdminResourceList>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<AdminResourceList>(admins,HttpStatus.OK);
    }

    @RequestMapping(value="/{adminId}",method=RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<AdminResource> findAdminById(
            @PathVariable Long adminId)throws IOException{
        AdminResource admin;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/admins/"+adminId);
        URI uri = null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        admin=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),AdminResource.class);
        if(admin.getRid()==0)
            return new ResponseEntity<AdminResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<AdminResource>(admin,HttpStatus.OK);
    }
}
