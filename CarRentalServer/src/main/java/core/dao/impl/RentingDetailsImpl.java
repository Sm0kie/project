package core.dao.impl;

import core.dao.RentingDetailsDao;
import core.models.entities.RentingDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
@Service
@Transactional
public class RentingDetailsImpl implements RentingDetailsDao {
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<RentingDetails> findAllRentingDetails() {
        return em.createQuery("SELECT a FROM RentingDetails a").getResultList();
    }

    @Override
    public RentingDetails findRentingDetailsById(Long id) {
        return em.find(RentingDetails.class,id);
    }

    @Override
    public List<RentingDetails> findRentingDetailsByClient(Long id) {
        Query query=em.createQuery("SELECT a FROM RentingDetails a WHERE a.client.id=?1");
        query.setParameter(1,id);
        return query.getResultList();
    }

    @Override
    public List<RentingDetails> findRentingDetailsByCar(Long id) {
        Query query=em.createQuery("SELECT a FROM RentingDetails a WHERE a.car.id=?1");
        query.setParameter(1,id);
        return query.getResultList();
    }

    @Override
    public RentingDetails createRentingDetails(RentingDetails data) {
        em.persist(data);
        em.flush();
        em.refresh(data);
        return data;
    }

    @Override
    public RentingDetails updateRentingDetails(Long rentingDetailsId, RentingDetails data) {
        RentingDetails rentingDetails=em.find(RentingDetails.class,rentingDetailsId);
        rentingDetails.setCar(data.getCar());
        rentingDetails.setClient(data.getClient());
        rentingDetails.setDropOffDate(data.getDropOffDate());
        rentingDetails.setPickUpDate(data.getPickUpDate());
        rentingDetails.setPickUpHour(data.getPickUpHour());
        rentingDetails.setDropOffHour(data.getDropOffHour());
        rentingDetails.setPickUpLocation(data.getPickUpLocation());
        rentingDetails.setReturnLocation(data.getReturnLocation());
        em.merge(rentingDetails);
        return rentingDetails;
    }

    @Override
    public RentingDetails removeRentingDetails(Long id) {
        RentingDetails rentingDetails=em.find(RentingDetails.class,id);
        em.remove(rentingDetails);
        return rentingDetails;
    }
}
