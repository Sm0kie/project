package rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.models.Car;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.CarResource;
import rest.resources.CarResourceList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
public class CarController {

    @RequestMapping(value = "/cars",method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<CarResourceList> findAllCars() throws IOException {
        CarResourceList cars;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/cars");
        URI uri = null;
        try {
            uri = builder.build();
        } catch (URISyntaxException e) {
        }
        HttpResponse response = client.execute(new HttpGet(uri));
        cars = new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())), CarResourceList.class);
        return new ResponseEntity<CarResourceList>(cars, HttpStatus.OK);
    }

    @RequestMapping(value = "/cars/{carId}", method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<CarResource> findCarById(
            @PathVariable Long carId) throws IOException {
        CarResource car;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/cars/" + carId);
        URI uri = null;
        try {
            uri = builder.build();
        } catch (URISyntaxException e) {
        }
        HttpResponse response = client.execute(new HttpGet(uri));
        car = new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())), CarResource.class);
        if (car.getRid() == 0)
            return new ResponseEntity<CarResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<CarResource>(car, HttpStatus.OK);
    }

    @RequestMapping(value = "/cars/{carId}", method = RequestMethod.DELETE)
    @PreAuthorize("permitAll")
    public ResponseEntity<CarResource> removeCar(
            @PathVariable Long carId) throws IOException {
        CarResource res;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/cars/" + carId);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response=client.execute(new HttpDelete(uri));
        res=new Gson().fromJson((JsonObject)new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),CarResource.class);
        return new ResponseEntity<CarResource>(res,HttpStatus.OK);
    }

    @RequestMapping(value = "/cars",method = RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<CarResource> addCar(
            @RequestBody Car data,
            UriComponentsBuilder ucBuilder) throws IOException {
        CarResource car;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/cars");
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpPost request=new HttpPost(uri);
        StringEntity stringEntity=new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(data))).toString(),HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response=client.execute(request);
        car=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),CarResource.class);
        if(car.getRid()==0)
            return new ResponseEntity<CarResource>(HttpStatus.CONFLICT);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/cars/{carId}").buildAndExpand(car.getId()).toUri());
        return new ResponseEntity<CarResource>(car,headers,HttpStatus.OK);
    }

    @RequestMapping(value = "/cars/{carId}", method = RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<CarResource> updateCar(
            @PathVariable Long carId,
            @RequestBody Car data) throws IOException {
        CarResource car;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder=new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/cars/"+carId);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpPost request = new HttpPost(uri);
        StringEntity stringEntity=new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(data))).toString(), HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response = client.execute(request);
        car=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),CarResource.class);
        if(car.getRid()==0)
            return new ResponseEntity<CarResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<CarResource>(car,HttpStatus.OK);
    }

    @RequestMapping(value = "/returnCar/{carId}", method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<CarResource> returnCar(
            @PathVariable Long carId) throws IOException {
        CarResource car;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/returnCar/" + carId);
        URI uri = null;
        try {
            uri = builder.build();
        } catch (URISyntaxException e) {
        }
        HttpResponse response = client.execute(new HttpGet(uri));
        car = new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())), CarResource.class);
        if (car.getRid() == 0)
            return new ResponseEntity<CarResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<CarResource>(car, HttpStatus.OK);
    }
}
