angular.module( 'ngBoilerplate.car', [
  'ui.router',
  'plusOne',
  'base64',
  'ngBoilerplate.login',
  'ngBoilerplate.home'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'manageCar', {
    url: '/cars/car?carId',
    views: {
      'main': {
        controller: 'CarManageCtrl',
        templateUrl: 'car/car.tpl.html'
      }
    },
    data:{ pageTitle: 'Car' },
    resolve:{
        car: function(carInfoService,$stateParams){
            return carInfoService.getCarById($stateParams.carId);
        }
    }
    })
    .state('updateCar',{
         url:'/cars/update/car?carId',
         views:{
            'main':{
                controller: 'UpdateCarCtrl',
                templateUrl:'car/update_car.tpl.html'
            }
         },
         data:{ pageTitle: 'Update Car Info'},
         resolve:{
            car: function(carInfoService,$stateParams){
                 return carInfoService.getCarById($stateParams.carId);
             }
        }
    });
})
.factory('carInfoService',function($resource,$http,$base64){
     var service={};
     service.getAllCars=function(){
             var Car=$resource("/CarRental/cars");
             return Car.get().$promise.then(function(data){
                 return data.cars;
             });
             };
     service.getCarById = function(carId) {
         var Car = $resource("/CarRental/cars/:paramCarId");
         return Car.get({paramCarId:carId}).$promise;
     };
     service.removeCar=function(carId){
        var Car=$resource("/CarRental/cars/:paramCarId");
        return Car.remove({paramCarId:carId}).$promise;
     };
     service.updateCarInfo=function(carId,updatedCar){
        var Car=$resource("/CarRental/cars/:paramCarId");
        return Car.save({paramCarId:carId},updatedCar).$promise;
     };
     service.addCar=function(savedCar){
        var Car=$resource("/CarRental/cars/");
        return Car.save(savedCar).$promise;
     };
     return service;
 })
 .controller('UpdateCarCtrl',function($scope,$state,carInfoService,car,sessionService){
      $scope.isLoggedIn = sessionService.isLoggedIn;
      $scope.logout = sessionService.logout;
      $scope.car=car;

      $scope.updateCarInfo=function(category,brand,model,doors,fuelType,engineSize,price,location,imageName){
         carInfoService.updateCarInfo(car.rid,{
             category: car.category,
             brand: car.brand,
             model: car.model,
             doors: car.doors,
             fuelType:car.fuelType,
             engineSize:car.engineSize,
             price:car.price,
             location:car.location,
             imageName:car.imageName,
             available:car.available
         }).then(function(){
             $state.go("manageCar",{carId:car.rid},{reload:true});
         });
      };
   })
.controller('CarManageCtrl',function($scope,$state,carInfoService,car,sessionService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
     $scope.car=car;
     $scope.removeCar=function(){
             carInfoService.removeCar(car.rid).then(function(){
                 $state.go("carSearch",{reload:true});
             });
             };
 });