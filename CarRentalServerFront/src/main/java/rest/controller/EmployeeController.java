package rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.models.Employee;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.EmployeeResource;
import rest.resources.EmployeeResourceList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by ilyes on 5/8/2016.
 */
@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {

    @RequestMapping(method= RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<EmployeeResourceList> findAllEmployees(
            @RequestParam(value="username",required=false) String username,
            @RequestParam(value="password",required=false) String password) throws IOException {
        EmployeeResourceList employees;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        if(username==null)
            builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/employees");
        else
            builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/employees")
                    .setParameter("username",username)
                    .setParameter("password",password);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        employees=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),EmployeeResourceList.class);
        return new ResponseEntity<EmployeeResourceList>(employees,HttpStatus.OK);
    }

    @RequestMapping(value="/{employeeId}",method=RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<EmployeeResource> findEmployeeById(
            @PathVariable Long employeeId) throws IOException {
        EmployeeResource employee;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/employees/"+employeeId);
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpResponse response = client.execute(new HttpGet(uri));
        employee = new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),EmployeeResource.class);
        if(employee.getRid()==0)
            return new ResponseEntity<EmployeeResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<EmployeeResource>(employee,HttpStatus.OK);
    }

    @RequestMapping(value="/{employeeId}",method=RequestMethod.DELETE)
    @PreAuthorize("permitAll")
    public ResponseEntity<EmployeeResource> removeEmployee(
            @PathVariable Long employeeId) throws IOException {
        EmployeeResource employee;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/employees/"+employeeId);
        URI uri = null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpDelete request = new HttpDelete(uri);
        HttpResponse response = client.execute(request);
        employee=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),EmployeeResource.class);
        if(employee.getRid()==0)
            return new ResponseEntity<EmployeeResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<EmployeeResource>(employee,HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<EmployeeResource> addEmployee(
            @RequestBody Employee data,
            UriComponentsBuilder ucBuilder) throws IOException {
        EmployeeResource employee;
        HttpClient client=new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/employees");
        URI uri=null;
        try{
            uri=builder.build();
        }catch(URISyntaxException e){}
        HttpPost request= new HttpPost(uri);
        StringEntity stringEntity = new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(data))).toString(),HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response=client.execute(request);
        employee=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),EmployeeResource.class);
        if(employee.getRid()==0)
            return new ResponseEntity<EmployeeResource>(HttpStatus.NOT_FOUND);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/employees/{employeeId}").buildAndExpand(employee.getId()).toUri());
        return new ResponseEntity<EmployeeResource>(employee,headers,HttpStatus.OK);
    }

    @RequestMapping(value="/{employeeId}",method=RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<EmployeeResource> updateEmployee(
            @PathVariable Long employeeId,
            @RequestBody Employee data) throws IOException {
        EmployeeResource employee;
        HttpClient client = new DefaultHttpClient();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost("localhost:8081/CarRentalServer").setPath("/employees/"+employeeId);
        URI uri = null;
        try{
            uri = builder.build();
        }catch(URISyntaxException e){}
        HttpPost request = new HttpPost(uri);
        StringEntity stringEntity = new StringEntity(((JsonObject) new JsonParser().parse(new ObjectMapper().writeValueAsString(data))).toString(),HTTP.UTF_8);
        stringEntity.setContentType("application/json");
        request.setEntity(stringEntity);
        HttpResponse response=client.execute(request);
        employee=new Gson().fromJson((JsonObject) new JsonParser().parse(new InputStreamReader(response.getEntity().getContent())),EmployeeResource.class);
        if(employee.getRid()==0)
            return new ResponseEntity<EmployeeResource>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<EmployeeResource>(employee,HttpStatus.OK);
    }
}
