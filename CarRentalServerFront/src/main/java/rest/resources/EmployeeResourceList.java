package rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 5/8/2016.
 */
public class EmployeeResourceList extends ResourceSupport {
    private List<EmployeeResource> employees=new ArrayList<EmployeeResource>();

    public List<EmployeeResource> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeResource> employees) {
        this.employees = employees;
    }
}
