angular.module( 'ngBoilerplate.employee_home', [
  'ui.router',
  'plusOne',
  'ngBoilerplate.login',
  'ngBoilerplate.employee'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'employee_home', {
    url: '/employee_home/rentingDetails',
    views: {
      'main': {
        controller: 'EmployeeHomeCtrl',
        templateUrl: 'employee_home/employee_home.tpl.html'
      }
    },
    resolve: {
               rentingDetails: function(rentingDetailsInfoService) {
                   return rentingDetailsInfoService.getAllRentingDetails();
               }
           },
    data:{ pageTitle: 'Renting Details' }
    });
})
.factory('rentingDetailsInfoService',function($resource){
      var service={};
      service.getRentingDetailsById = function(rentingDetailsId) {
          var RentingDetails= $resource("/CarRental/rentingDetails/:paramRentingDetailsId");
          return RentingDetails.get({paramRentingDetailsId:rentingDetailsId}).$promise;
      };
      service.getAllRentingDetails=function(){
          var RentingDetails=$resource("/CarRental/rentingDetails");
          return RentingDetails.get().$promise.then(function(data){
              return data.rentingDetails;
          });
      };
      service.addRentingDetails=function(carId,savedDetails){
          var RentingDetails=$resource("/CarRental/addRentingDetails/:paramCarId");
          return RentingDetails.save({paramCarId:carId},savedDetails).$promise;
       };
       service.updateRentingDetailsInfo=function(rentingDetailsId,updatedRentingDetails){
          var RentingDetails=$resource("/CarRental/rentingDetails/:paramRentingDetailsId");
          return RentingDetails.save({paramRentingDetailsId:rentingDetailsId},updatedRentingDetails).$promise;
       };
        service.removeRentingDetails=function(rentingDetailsId){
           var RentingDetails=$resource("/CarRental/rentingDetails/:paramRentingDetailsId");
           return RentingDetails.remove({paramRentingDetailsId:rentingDetailsId}).$promise;
        };
        service.createContract=function(contractId){
            var Contract=$resource("/CarRental/contract/:paramContractId");
            return Contract.save({paramContractId:contractId},{}).$promise;
        };
        service.returnCar=function(carId){
            var Car = $resource("/CarRental/returnCar/:paramCarId");
            return Car.get({paramCarId:carId}).$promise;
        };
      return service;
  })
.controller( 'EmployeeHomeCtrl', function HomeController($http, $window,$state,$scope, sessionService,$resource,rentingDetails,carInfoService,rentingDetailsInfoService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
         sessionService.logout();
         $state.go("login",{reload:true});
    }

    $scope.rentingDetails=rentingDetails;
    $scope.dataCar={
               repeatCar: null,
               availableCar: rentingDetails
              };
    $scope.carFilter=function(item){
        if(item.car.available===true){
            return false;
        }
        return true;
    };

    $scope.myFilter=function(item){
        if(item.available===false){
            return false;
        }
        return true;
    };
    $scope.returnCar=function(){
        rentingDetailsInfoService.returnCar($scope.dataCar.repeatCar).then(function(){
                     $state.go($state.current,{},{reload:true});
                 });
    };
    $scope.createContract=function(rentingDetailId){
        rentingDetailsInfoService.createContract(rentingDetailId.rentingDetailId).then(function(){
                     var win=window.open("http://localhost:8080/CarRental/exportPdf/"+rentingDetailId.rentingDetailId,'_blank');
                     try{
//                     win.location.href='http://localhost:8080/CarRental/exportPdf/'+rentingDetailId.rentingDetailId;
                     win.focus();
                     }catch(e){
                      win.close();
                      return false;
                     }
        });
        $state.go($state.current,{},{reload:true});
    };
  });